<?php
$h=$_SERVER["HTTP_HOST"];
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Acceso Suspendido</title>
	<!--<link href="css/style.css" rel="stylesheet" type="text/css" />-->
	<style>
*{
	margin:0;
	padding:0;
	list-style:none;
}
body {
	font: 70% "Lucida Grande","Lucida Sans Unicode",geneva,verdana,sans-serif;
	margin:10px;
	/*background-color:#3c81b1;*/
	color:#555;
}
html { 
	height: 100%; 
	margin-bottom: 1px; 
}
.header {
	background: #000;
	cursor: pointer;
	padding:4px;
}
#footer {
	width:660px;
	margin:0 auto;
	color:#ccc;
}
#footer a {
	color:#aaa;
	text-decoration:none;
	font-weight:bold;
}
#footer .validate {
	float:right;
}
.header:hover{
	background-color:#666;
}
#header h1 a {
	color:#333;
	text-decoration:none;
}
#header h1 a:hover {
	color:#000;
}
.selected{
	background-color:#F00;
	color:#FFF;
}
.content{
	background:#FFF;
	padding:4px;
}
.accordion_child a {
	color:#333;
}
.accordion_child a:hover {
	text-decoration:none;
}
.accordion_child ul.links li {
	background:url(../images/link.gif) left no-repeat;
}
input {
	width:180px;
	/*background:url(http://img638.imageshack.us/img638/2038/input.jpg) top no-repeat;*/
	background:url(http://i.imgur.com/6nYkc4J.jpg) top no-repeat;
	font: 100% "Lucida Grande","Lucida Sans Unicode",geneva,verdana,sans-serif;
	color:#333;
	border:0;
	padding:7px 10px;
	margin:5px;
	color:#777;
}
textarea {
	width:350px;
	height:105px;
/*	background:url(http://img638.imageshack.us/img638/1736/textarea.jpg) top no-repeat; */
	background:url(http://i.imgur.com/nchiIHN.jpg) top no-repeat;;
	font: 100% "Lucida Grande","Lucida Sans Unicode",geneva,verdana,sans-serif;
	color:#333;
	border:0;
	padding:10px;
	margin:5px;
	color:#777;
}
input[type=submit] {
	 cursor:pointer;
	 background:#ff9900;
	 color:#fff;
	 font-weight:bold;i lo desea contacte al Administrador de Sistemas.
	 width:182px;
	 height:33px;
	 padding:7px 10px;
	 /*background:url(http://img638.imageshack.us/img638/1662/submita.jpg) top no-repeat;*/
	 background:url(http://i.imgur.com/68hkqTP.jpg) top no-repeat;
}
blockquote {
	  font: 1.5em/1.8em Arial, "Georgia", Times, serif;
	  /*background: url(http://img195.imageshack.us/img195/8765/closequote.gif) no-repeat right bottom;*/
	  background: url(http://i.imgur.com/FdmoVRs.gif) no-repeat right bottom;
	  padding-left: 18px;
	  text-indent: -18px;
	  color:#888;
	  margin:20px 0;
}
blockquote:first-letter {
	/*background: url(http://img195.imageshack.us/img195/1408/openquote.gif) no-repeat left top;*/
	background: url(http://i.imgur.com/HK7tufS.gif) no-repeat left top;
    padding-left: 18px;
}
.accordion_child ul {
	margin:25px 0 25px 0px;
}
.accordion_child ul li {
	list-style:none;
	padding-left:20px;
	margin:10px 0;
	background:url(../images/bullet.gif) left no-repeat;
	border-bottom:1px solid #eee;
	padding-bottom:10px;
	line-height:19px;
}
table {
	width:100%;
	padding:5px;
}
td {
	background:#eee;
	padding:4px;
	text-align:center;
	/*background:url(http://img195.imageshack.us/img195/5065/95342258.jpg) top repeat-x;*/
	background:url(http://i.imgur.com/fjrkzWx.jpg) top repeat-x;
}
h2 {
clear:both;
}
.accordion_child h2 {
	font:170% Arial;
	margin:10px 0 20px;
	padding:10px 0;
	color:#B02700;
	border-bottom:1px solid #eee;
	/*background:url(http://img195.imageshack.us/img195/1624/10619984.jpg) bottom left no-repeat;*/
	background:url(http://i.imgur.com/5Y1GbPB.jpg) bottom left no-repeat;
}
#header h1 {
text-align:center;
font:350% Arial;
color:#232323;
margin-top:50px;
letter-spacing:-1px;
}
#header h2 {
text-align:center;
font:100% Verdana;
text-transform:uppercase;
letter-spacing:2px;
color:#bbb;
margin:10px 0 40px;
}
#accordian{
	padding:5px;
	width:660px;
	z-index:2;
	margin:0 auto;
}
.accordion_headings{
	padding:7px 10px;
	/*background:url(http://img195.imageshack.us/img195/5585/itembg.jpg) top no-repeat;*/
	background:url(http://i.imgur.com/qCBNQQ1.jpg) top no-repeat;
	color:#FFFFFF;
	margin-top:2px;
	cursor:pointer;
	font:170% Trebuchet MS;
	letter-spacing:1px;
	clear:both;
}
.accordion_headings:hover{
	/*background:url(http://img52.imageshack.us/img52/9164/itembghover.jpg) top no-repeat;*/
	background:url(http://i.imgur.com/9FbMsdo.jpg) top no-repeat;
}
.accordion_child{
	padding:20px 30px;
	background:#fafafa;
	border:5px solid #f4f4f4;
}
img {
	float:right;
	padding:5px;
	background:#fff;
	margin:0 0 20px 20px;
	border:1px solid #eee;
}
#pie img
{
	float:left;
	padding:0px;
	background:#fff;
	margin:0 0 0px 0px;
	border:1px solid #eee;
}
p {
	margin:15px 0;
	line-height:19px;
}
.header_highlight{
	/*background:url(http://img52.imageshack.us/img52/9164/itembghover.jpg) top no-repeat;*/
	background:url(http://i.imgur.com/9FbMsdo.jpg) top no-repeat;
}
	</style>
</head>
<body onload="new Accordian('accordian',3,'header_highlight');">
<div id="header">
<h1><strong>Acceso suspendido - <?php print $_SERVER["REMOTE_ADDR"]; ?> </strong></h1>
</div>
<div id="accordian" >
  <div id="test-header" class="accordion_headings header_highlight" >El acceso a la página</div>
  <div id="test-content">
    <div class="accordion_child">
		<!-- IMAGEN DE LA EMPRESA -->
    	<img height="70" src="http://i.imgur.com/hkSkhLE.jpg" />
		<p><h3><a href="http://<?php print $h; ?>" target="_blank">http://<?php print $h; ?></a></h3></p>
		<!-- <p><h3><a href="http://<?php print $h; ?>">http://<?php print $h; ?></a></h3></p> -->
		<p>
		<h3>Ha sido denegado por la siguiente raz&oacute;n: <b><i>Déficit de pago.</i></b> </h3>
		<h2 align="center"><strong><font color="#ff0000">Sitio denegado: <?php print $h; ?></font></strong></h2>	
	<!--	<h2 align="center"><strong><font color="#ff0000">Sitio no permitido: <?php print $h; ?></font></strong></h2> -->	
		</p>
		
		<p>
		    <blockquote> Ud. está viendo este mensaje de error porque se ha detectado<br> 
				retraso en el pago de renta del servico Firewall XpertNTC.</blockquote>
		</p>
		<p>
			<center><h3>Contacte al administrador de sistemas.</h3></center><br>			
		</p>
	</div>
  </div>
</div>
<div id="accordian" >
		<p>
			<hr>
			<div id="pie">
			<!-- LOGO de Powered by XpertNTC -->
			<p><img height="40" src="http://i.imgur.com/6nh1P33.jpg" />			
			</p> 			
				</div>
		</p>
</div>
		
</body>
</html>
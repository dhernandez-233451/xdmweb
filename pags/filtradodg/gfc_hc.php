<?php
    include_once("../cabecera.php");

$ruta="/var/log/dansguardian/";
$archivo="access.log";
#$ruta="/wwnetworks/xadmon/pags/filtradodg/ap/logs/";
#$archivo="access.log";
$datos_ga=null;
$datos_a=null;
$datos_t=null;
if(isset($_POST['slc_tipo'])){
    #tail -n 1 /var/log/dansguardian/access.log
    $fd=trim(shell_exec('sudo date +%d'));
    if($fd<10)$fd=str_replace("0", "", $fd);
    $fm=trim(shell_exec('sudo date +%m'));
    if($fm<10)$fm=str_replace("0", "", $fm);
    $fa=trim(shell_exec('sudo date +%Y'));
    $fecha=$fa.".".$fm.".".$fd;
    #$hora=trim(shell_exec('sudo date +%H'));
    $h=trim(shell_exec('sudo tail -n 1 '.$ruta.$archivo.' |awk \'{print $2}\''));
    $xh=explode(":", $h);
    $hora=trim($xh[0]);
    #echo $fecha;
    exec('sudo  cat '.$ruta.$archivo.'.1',$resultado);
    exec('sudo cat '.$ruta.$archivo,$resultado);
    exec('sudo ls /etc/dansguardian/lists/ |grep "[A-Z]"',$grupos);
    #print_r($resultado);
    $grupos[]="Standard";
    #$grupos[]="ADMIN";#de prueba---quitar----
    $datos=null;
    for($i=0;$i<=$hora;$i++)foreach ($grupos as $nombre) $datos[$i][$nombre]["e"]=$datos[$i][$nombre]["b"]=0;
    #print_r($datos);
    foreach ($resultado as $registro) {
        $partes=explode(" ", $registro);
        $sumar=false;
        if($partes[0]==$fecha){
            $xh=explode(":", $partes[1]);
            $h=$xh[0];
            foreach ($grupos as $nombre)if(preg_match("/(".$nombre.")/", $registro)){
                $g=$nombre;
                break;
            }
            if(preg_match("/(EXCEPTION)/", $registro)){
                $a="e";
                $sumar=true;
            }
            else if(preg_match("/(DENIED)/", $registro)){
                $a="b";
                $sumar=true;
            }
            if($sumar)$datos[$h][$g][$a]++;
        }
    }
    switch ($_POST['slc_tipo']) {
        case 2:
            foreach ($grupos as $nombre) $datos_ga[$nombre]["e"]=$datos_ga[$nombre]["b"]=0;
            foreach ($grupos as $nombre)for($i=0;$i<=$hora;$i++){
                $datos_ga[$nombre]["e"]+=$datos[$i][$nombre]["e"];
                $datos_ga[$nombre]["b"]+=$datos[$i][$nombre]["b"];
            }
            #print_r($datos_ga);
            break;
        case 3:
            $datos_a["e"]=$datos_a["b"]=0;
            for($i=0;$i<=$hora;$i++)foreach ($grupos as $nombre){
                $datos_a["e"]+=$datos[$i][$nombre]["e"];
                $datos_a["b"]+=$datos[$i][$nombre]["b"];
            }
            #print_r($datos_a);
            break;
        case 4:
            for($i=0;$i<=$hora;$i++)$datos_t[$i]["e"]=$datos_t[$i]["b"]=0;
            for($i=0;$i<=$hora;$i++)foreach ($grupos as $nombre){
                $datos_t[$i]["e"]+=$datos[$i][$nombre]["e"];
                $datos_t[$i]["b"]+=$datos[$i][$nombre]["b"];
            }
            #print_r($datos_t);
            break;
        default:
            # code...
            break;
    }
}
?>

<HTML>
<head>
    <title>Monitor gráfico</title>
</head>
<BODY>

<h2 align="center">Gráfica de firltrado de contenido</h2>
<form id="tipog" name="tipog" method="post" action="">
    Gráfica tipo: 
    <select id="slc_tipo" name="slc_tipo" onchange="this.form.submit()">
        <option value="1" <?php if(isset($_POST['slc_tipo'])&&$_POST['slc_tipo']==1)print 'selected="selected"'; ?>>Elegir...</option>
        <option value="2" <?php if(isset($_POST['slc_tipo'])&&$_POST['slc_tipo']==2)print 'selected="selected"'; ?>>Grupos</option>
        <option value="3" <?php if(isset($_POST['slc_tipo'])&&$_POST['slc_tipo']==3)print 'selected="selected"'; ?>>Cantidad de acciones</option>
        <option value="4" <?php if(isset($_POST['slc_tipo'])&&$_POST['slc_tipo']==4)print 'selected="selected"'; ?>>Tiempo</option>
    </select>
</form>
<div id="contenedor"></div>

<!--<script src="https://code.jquery.com/jquery.js"></script>-->
<!--<script src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>-->
<script src="../../js/jquery-3.2.1.js"></script>
<script src="../../js/highcharts.js"></script>
<script src="../../js/exporting.js"></script>
<script>
<?php if(isset($_POST['slc_tipo'])){
    switch ($_POST['slc_tipo']) {
        case 2:
            print 'graf1();';
            break;
        case 3:
            print 'graf2();';
            break;
        case 4:
            print 'graf3();';
            break;
        default:
            # code...
            break;
    }
} ?>
function graf1(){
    chartCPU = new Highcharts.Chart({
        chart: {
            renderTo: 'contenedor',
            type: 'column'
        },
        rangeSelector : {
            enabled: false
        },
        title: {
            text: 'Filtrado de contenido por grupos'
        },
        xAxis: {
            categories: [<?php  for($i=0;$i<count($grupos);$i++)
            if($i==(count($grupos)-1))print '\''.$grupos[$i].'\'';
            else print '\''.$grupos[$i].'\',';?>],
            crosshair: true,
            title: {
                text: 'Grupos'
            }
        },
        yAxis: {
            minPadding: 0.2,
            maxPadding: 0.2,
            title: {
                text: 'Cantidad',
                margin: 10
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            series: {
                marker: {
                    enabled: false
                }
            },
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Excepciones',
            color: '#FF9B36',
            showInNavigator: true,
            data: (function() {
                var data = [];
                <?php foreach ($grupos as $nombre){ ?>
                data.push([<?php echo $datos_ga[$nombre]["e"];?>]);
                <?php } ?>
                return data;
            })()
        },{   
            name: 'Bloqueos',
            color: '#203B86',
            data: (function(){
                var data=[];
                <?php foreach ($grupos as $nombre){ ?>
                data.push([<?php  echo $datos_ga[$nombre]["b"];?>]);
                <?php  } ?>
                return data;
            })()
            }],
        credits: {
            enabled: false
        },
        lang : {
              printChart: 'Imprimir',
              downloadJPEG: 'Descargar en JPEG',
              downloadPDF:  'Descargar en PDF',
              downloadPNG:  'Descargar en PNG',
              downloadSVG: '',
        }
    });
}
function graf2(){
    chartCPU = new Highcharts.Chart({
        chart: {
            renderTo: 'contenedor',
            type: 'column'
        },
        rangeSelector : {
            enabled: false
        },
        title: {
            text: 'Cantidad de datos filtrados'
        },
        xAxis: {
            type: "category",
            title: {
                text: 'Acciones'
            }
        },
        yAxis: {
            minPadding: 0.2,
            maxPadding: 0.2,
            title: {
                text: 'Cantidad',
                margin: 10
            }
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.f}'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f} </b> en total<br/>'
        },
        series: [{
            name: 'Excepciones',
            color: '#FF9B36',
            showInNavigator: true,
            data: (function() {
                var data = [];
                data.push([<?php echo $datos_a["e"];?>]);
                return data;
            })()
            },{   
            name: 'Bloqueos',
            color: '#203B86',
            data: (function(){
                var data=[];
                data.push([<?php echo $datos_a["b"];?>]);
                return data;
            })()
            }],
        credits: {
            enabled: false
        },
        lang : {
              printChart: 'Imprimir',
              downloadJPEG: 'Descargar en JPEG',
              downloadPDF:  'Descargar en PDF',
              downloadPNG:  'Descargar en PNG',
              downloadSVG: '',
        }
    });
}
function graf3(){
    chartCPU = new Highcharts.Chart({
        chart: {
            renderTo: 'contenedor',
            type: 'line'
        },
        rangeSelector : {
            enabled: true
        },
        title: {
            text: 'Acciones por hora'
        },
        xAxis: {
            //type: "category",
            categories: [<?php for($i=0;$i<=$hora;$i++)
                if($i!=$hora)print '\''.$i.':00\',';
                else print '\''.$i.':00\'';?>],
            title: {
                text: 'Horas'
            }
        },
        yAxis: {
            minPadding: 0.2,
            maxPadding: 0.2,
            title: {
                text: 'Cantidad',
                margin: 10
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Excepciones',
            color: '#FF9B36',
            showInNavigator: true,
            data: (function() {
                var data = [];
                <?php  for($i = 0 ;$i<count($datos_t);$i++){ ?>
                data.push([<?php echo $datos_t[$i]["e"]; ?>]);
                <?php } ?>
                return data;
            })()
        },{   
            name: 'Bloqueos',
            color: '#203B86',
            data: (function(){
                var data=[];
                <?php for($i = 0 ;$i<count($datos_t);$i++){ ?>
                data.push([<?php echo $datos_t[$i]["b"];?>]);
                <?php } ?>              
                return data;
            })()
            }],
        credits: {
            enabled: false
        },
        lang : {
              printChart: 'Imprimir',
              downloadJPEG: 'Descargar en JPEG',
              downloadPDF:  'Descargar en PDF',
              downloadPNG:  'Descargar en PNG',
              downloadSVG: '',
        }
    });
}
</script>
<hr>
<form name="regresar" action="index.php">
        <button type="submit" style="background-color: #d9534f;">Regresar</button>
</form>
</BODY>
</html>
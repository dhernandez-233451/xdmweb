<?php 

function hdir($grupo){
	$ruta="/etc/dansguardian/lists/";
	exec('sudo mkdir '.$ruta.$grupo);
	exec('sudo mkdir '.$ruta.$grupo.'/lists');
	exec('sudo ls '.$ruta.' |grep -v "black\|~\|[A-Z]\|\."',$lista);
	foreach ($lista as $archivo)exec('sudo cp -R '.$ruta.$archivo.' '.$ruta.$grupo.'/lists/');
	exec('sudo chmod -R u=rwx,g=rwx,o=rwx '.$ruta.$grupo.'/lists/');
}
function hdgxconf($grupo,$f,$modo){
	$ruta="/etc/dansguardian/";
	$plantilla=trim(shell_exec('sudo ls '.$ruta.' |grep "conf\."'));
	$archivo="dansguardianf".$f.".conf";
	#exec('sudo cp '.$ruta.$plantilla.' '.$ruta.$archivo);
	#exec('sudo cat '.$ruta.$archivo,$contenido);
	exec('sudo cat '.$ruta.$plantilla,$contenido);
	$acumulador="";
	foreach ($contenido as $linea) {
		if(preg_match("/(groupmode)/",$linea))$acumulador.="groupmode = ".$modo."\n";
		else if(preg_match("/(groupname)/",$linea))$acumulador.="groupname = '".$grupo."' #".date("Y.n.d H:m")."\n";
		else if(preg_match("/( = '\/)/",$linea)){
			$x1=explode("=", $linea);
			$acumulador.=$x1[0]."= '";
			$x2=explode("/", $x1[1]);
			for ($i=1; $i < count($x2); $i++) { 
				if($i==(count($x2)-1))$acumulador.="/".$grupo."/lists";
				$acumulador.="/".$x2[$i];
			}
			$acumulador.="\n";
		}
		else $acumulador.=$linea."\n";
	}
	$instruccion='sudo echo "'.$acumulador.'" > '.$ruta.$archivo;
	exec($instruccion);
	exec('sudo chmod u=rx,g=rx,o=rx '.$ruta.$archivo);
	exec('sudo chown root:root '.$ruta.$archivo);
	$contenido=null;
	#print $acumulador;
}
function ngip($grupo,$rango,$f){
	$ruta="/etc/dansguardian/lists/authplugins/";
	$archivo="ipgroups";
	$acumulador="";
	exec('sudo cat '.$ruta.$archivo,$contenido);
	foreach ($contenido as $linea)if($linea!="")$acumulador.=$linea."\n";
	$acumulador.="#".$grupo."\n".$rango." = filter".$f;

	exec('sudo chmod u=rwx,g=rwx,o=rwx '.$ruta);
	exec('sudo chmod u=rw,g=rw,o=rw '.$ruta.$archivo);
	exec('sudo cat '.$ruta.$archivo.' > '.$ruta.$archivo.'.backup');
	$instruccion='sudo echo "'.$acumulador.'" > '.$ruta.$archivo;
	exec($instruccion);
	exec('sudo chmod u=rw,g=rw,o=rw '.$ruta.$archivo.'.backup');
	exec('sudo chmod u=rw,g=rw,o=rw '.$ruta.$archivo);
	exec('sudo chown root:root '.$ruta.$archivo.'.backup');
	exec('sudo chown root:root '.$ruta.$archivo);

	$contenido=null;
}
if(isset($_POST['txt_nombre'])){
	$ngrupo=$_POST['txt_nombre'];
	$modo=$_POST['slc_modo'];
	$rango=$_POST['txt_rango'];
	$filter=($_POST['hdn_grupos']+1);
	hdir($ngrupo);
	hdgxconf($ngrupo,$filter,$modo);
	ngip($ngrupo,$rango,$filter);
	header('Location: /pags/filtradodg/a_dgc.php?fg='.$filter);
}
?>
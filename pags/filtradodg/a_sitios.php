<?php 
	include_once("../cabecera.php");
$grupo="";
$ruta="";
$archivo="";
$type="";
#r  d rwx r-x r-x
#r1 d rwx r-x r-x
#a  - rwx r-x r-x
#a1 - rw- r-- r-- 
if (isset($_POST['hdn_grupo'])){
	$grupo=$_POST['hdn_grupo'];
	if($_POST['slc_exception']==$_POST['slc_banned']&&$_POST['slc_exception']=="")header('Location: /pags/filtradodg/sitios.php?grupo='.$grupo);

	$ruta="/etc/dansguardian/lists/".$grupo."/lists/";
	$ruta1="/etc/dansguardian/lists/";
	if($grupo=="Standard")$ruta=$ruta1;
	if($_POST['slc_exception']=="")$archivo=$_POST['slc_banned'];
	if($_POST['slc_banned']=="")$archivo=$_POST['slc_exception'];
}
else if(isset($_POST['lineas'])&&$_POST['lineas']!=""){
	$grupo=$_POST['grupo'];

	$ruta="/etc/dansguardian/lists/".$grupo."/lists/";
	$ruta1="/etc/dansguardian/lists/";
	if($grupo=="Standard")$ruta=$ruta1;
	$archivo=$_POST['archivo'];
	$type=$_POST['type'];

	exec('sudo cat '.$ruta.$archivo,$texto);
	$acumulador="";
	foreach ($texto as $renglon) {
		$acumulador.=$renglon."\n";
		if(preg_match("/^(#+)(LISTA)(.*)(#+)$/",$renglon))break;
	}
	for ($i=0; $i <=$_POST['lineas']; $i++) { 
		if(isset($_POST['txt_'.$i.'_'])&&$_POST['txt_'.$i.'_']!=""){
			if(!isset($_POST['chk_'.$i.'_']))$acumulador.="#";
			switch ($type) {
				case 'Extensión':
					$acumulador.=".".$_POST['txt_'.$i.'_'];
					break;
				case 'Frase':
					$acumulador.="<".$_POST['txt_'.$i.'_'].">";
					break;
				default:
					$acumulador.=$_POST['txt_'.$i.'_'];
					break;
			}
			if(isset($_POST['txth_'.$i.'_'])&&$_POST['txth_'.$i.'_']!="")$acumulador.=" # ".$_POST['txth_'.$i.'_'];
			$acumulador.="\n";
		}
	}
	$acumulador=trim($acumulador);

	exec('sudo chmod u=rwx,g=rwx,o=rwx '.$ruta);
	exec('sudo cat '.$ruta.$archivo.' > '.$ruta.$archivo.'.backup');
	$instruccion='sudo echo "'.$acumulador.'" > '.$ruta.$archivo;
	exec($instruccion);
	exec('sudo chmod u=rwx,g=rwx,o=rwx '.$ruta.$archivo.'.backup');
	exec('sudo chmod u=rwx,g=rwx,o=rwx '.$ruta.$archivo);
	exec('sudo chown root:root '.$ruta.$archivo.'.backup');
	exec('sudo chown root:root '.$ruta.$archivo);

	echo "<b>Actualización correcta</b></br>";
	echo "<b><i>Reiniciando servicios...</i></b></br>";
    print shell_exec('sudo /etc/init.d/dansguardian reload')."</br>";

	#print $acumulador;
}


?>
<!DOCTYPE html>
<html>
<head>
	<title>Filtrado de sitios-<?php print $grupo; ?></title>
</head>
<body>
<?php
function metiqueta($nombre){
	print '<label>'.$nombre.'</label>';
}
function mcaja($l,$nombre,$valor){
	print '<input type="text" id="txt_'.$l.'_'.$nombre.'" name="txt_'.$l.'_'.$nombre.'" value="'.$valor.'">';
}
function mcajab($l,$valor){
	print '<input type="text" readonly="readonly" id="txt_'.$l.'_" name="txt_'.$l.'_" value="'.$valor.'">';
}
function mco($l,$nombre,$valor){
	print '<input type="hidden" id="txth_'.$l.'_'.$nombre.'" name="txth_'.$l.'_'.$nombre.'" value="'.$valor.'">';
}
function mverificador($l,$nombre,$comentario){
	print ' Activar: <input type="checkbox" id="chk_'.$l.'_'.$nombre.'" name ="chk_'.$l.'_'.$nombre.'"';
	if(!$comentario)print ' checked="checked"';
	print '/>';
}
function mnota($l,$cadena,$xc){
	#echo $cadena;
	print '<input type="hidden" id="hdn_'.$l.'" name="hdn_'.$l.'" value="'.$xc.'" >';
}

	$nombresb=array(array("extension","Extensión"),array("ip","IP"),array("mimetype","MIME"),array("phrase","Frase"),array("regexpheader","Encabezado Regex"),array("regexpurl","URL Regex"),array("site","Sitio"),array("url","URL"));
	$nombrese=array(array("extension","Extensión"),array("filesite","Sitio de archivos"),array("fileurl","URL de archivos"),array("ip","IP"),array("mimetype","MIME"),array("phrase","Frase"),array("regexpurl","URL Regex"),array("site","Sitio"),array("url","URL"));
	if(preg_match("/(banned)/", $archivo)){
		$seleccion=str_replace(array("exception","banned","list"), "", $archivo);
		foreach ($nombresb as $nb) if($seleccion==$nb[0]){
			$type=$nb[1];
			$accion ="Denegar ".$type;
			break;
		}
	}
	if(preg_match("/(exception)/", $archivo)){
		$seleccion=str_replace(array("exception","banned","list"), "", $archivo);
		foreach ($nombrese as $ne) if($seleccion==$ne[0]){
			$type=$ne[1];
			$accion ="Permitir ".$type;
			break;
		}
	}


#Recuperación############################
$tipo="sitios";							#
include_once("../../pags/lnk_rec.php"); #
#########################################
	exec('sudo cat '.$ruta.$archivo,$contenido);
?>
<h2 align="center"><?php print $grupo; ?></h2>
<h3><?php print $accion;?></h3>

<form id="frm_sitios" name="frm_sitios" method="post" action="">
<?php 
$lineas=0;
for ($i=0; $i <count($contenido) ; $i++) { 
	if(preg_match("/^(#+)(LISTA)(.*)(#+)$/", $contenido[$i])){
		$i++;
		for ($i; $i <count($contenido); $i++) { 
			if($contenido[$i]!=""){
				print '<p>';
				$linea=$contenido[$i];
				$comentario=false;
				if(preg_match("/^#/", $linea))$comentario=true;
				$xlinea=str_replace("#", "", $linea);
				$palabras=explode(" ", $xlinea);
				foreach ($palabras as $palabra)if($palabra!=""){
					mverificador($lineas,"",$comentario);
					if($archivo=="exceptionextensionlist"||$archivo=="bannedextensionlist")mcajab($lineas,str_replace(".", "", $palabra));
					else{
						mcaja($lineas,"",($type=="Frase")?str_replace(array("<",">"), "", $palabra):$palabra);	
					}
					if(preg_match("/(.+)#(.*)/", $linea)){
						$palabras=explode("#", $linea);
						$coment=trim($palabras[(count($palabras)-1)]);
						metiqueta($coment);
						mco($lineas,"",$coment);
					}
					$lineas++;
					break;
				}
				print '</p>';
			}
		}
	}
}
if($archivo!="exceptionextensionlist"&&$archivo!="bannedextensionlist"){
	print '<p>Nuevo:</br>';
	mverificador($lineas,"",false);
	mcaja($lineas,"","");
	print '</p>';
}
?>
<input type="hidden" name="grupo" id="grupo" value="<?php print $grupo; ?>">
<input type="hidden" name="archivo" id="archivo" value="<?php print $archivo; ?>">
<input type="hidden" name="type" id="type" value="<?php print $type; ?>">
<input type="hidden" name="lineas" id="lineas" value="<?php print $lineas; ?>">
<button>Guardar</button>
</form>
<hr>
<form name="regresar" action="sitios.php?grupo=<?php print $grupo; ?>">
	<input type="hidden" name="grupo" id="grupo" value="<?php print $grupo; ?>">
		<button type="submit" style="background-color: #d9534f;">Regresar</button>
</form>
</body>
</html>
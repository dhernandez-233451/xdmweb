<?php 	include_once("../cabecera.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Filtrado de contenido</title>
</head>
<body>
<?php 
	$ruta="/etc/dansguardian/";
	$archivo="dansguardian.conf";

	exec('sudo ls '.$ruta.'lists/ |grep "[A-Z]"',$grupos);
	$filterport=shell_exec('sudo cat '.$ruta.$archivo.' |grep filterport |awk \'{print $3}\'');
	$proxyport=shell_exec('sudo cat '.$ruta.$archivo.' |grep proxyport |awk \'{print $3}\'');
	$filtergroups=shell_exec('sudo cat '.$ruta.$archivo.' |grep \'filtergroups \' |awk \'{print $3}\'');
	$proxyip=shell_exec('sudo cat '.$ruta.$archivo.' |grep proxyip |awk \'{print $3}\'');
	$proxyip=str_replace("'", "", $proxyip);

 ?>
<h2 align="center">Filtrado de contendio</h2>
<table border="1" align="right">
	<tr>
		<td><a href="gfc_hc.php">Monitor gráfico</a></td>
		<td><a href="control.php?t=1">Reiniciar</a></td>
		<td><a href="control.php?t=2">Detener</a></td>
		<td><a href="control.php?t=3">Iniciar</a></td>
	</tr>
</table>
<h3>Grupos existentes</h3>
 <table border="0">
 	<?php
 	foreach ($grupos as $grupo) print '<tr>
 		<td><b>'.$grupo.'</b></td>
 		<td>
 		Editar <a href="gruposip.php?grupo='.$grupo.'">Rangos o IPs del grupo</a> Editar <a href="sitios.php?grupo='.$grupo.'">Sitios Filtrados</a>
 		</td>
 		<td><a href="eliminar.php?grupo='.$grupo.'"><i>Eliminar grupo</i></a></td>
 		</tr>';
 	?>
 	<tr>
 		<td><b>Standard</b></td>
 		<td>
 		Editar <a href="gruposip.php?grupo=Standard">Rangos o IPs del grupo</a> Editar <a href="sitios.php?grupo=Standard">Sitios Filtrados</a>
 		</td>
 		<td><!--<a href=""><i>Eliminar grupo</i></a>--></td>
 		</tr>
 </table>
 <hr>
<h3>Agregar nuevo grupo</h3>
<form id="frm_ng" name="frm_ng" method="post" action="ngrupo.php">
	<table>
	<tr>
	<td>Grupos existentes: </td><td><?php print (count($grupos)+1); ?></td>
	</tr>
	<tr>
	<td>Nombre del grupo: </td><td><input type="text" name="txt_nombre" id="txt_nombre" onkeyup="this.value=this.value.toUpperCase();"/></td>
	</tr>
	<tr>
	<!--<td>Puerto del filtro: </td><td><input type="text" name="txt_puertof" id="txt_puertof" value="39999" /></td>
	</tr>
	<tr>
	<td>IP del proxy: </td><td><input type="text" name="txt_ipp" id="txt_ipp" value="127.0.0.1"/></td>
	</tr>
	<tr>
	<td>Puerto del proxy: </td><td><input type="text" name="txt_puertop" id="txt_puertop" value="39998"/></td>
	</tr>-->
	<tr>
	<td>Modo del grupo: </td><td><select id="slc_modo" name="slc_modo">
		<option value="0">Prohibido</option>
		<option value="1">Filtrado</option>
		<option value="2">Sin filtrar (excepción)</option>
	</select></td>
	</tr>
	<tr>
	<td>IP o segmento o rango de red</br>que pertenecerá al grupo:</td>
	<td><input type="text" name="txt_rango" id="txt_rango"/></td>
	</tr>
	<tr>
	<td>
	<input type="hidden" name="hdn_grupos" id="hdn_grupos" value="<?php print (count($grupos)+1); ?>">
	<button>Crear</button></td>
	</tr>
	</table>
</form>
<hr>
<h3>Configurar filtrado de contenido</h3>
<?php 
#Recuperación############################
$tipo="gruposip";						#
$ruta="/etc/dansguardian/";				#
$archivo="dansguardian.conf";			#
include_once("../../pags/lnk_rec.php"); #
#########################################
?>
<form id="frm_cfg" name="frm_cfg" method="post" action="a_dgc.php"><table>
	<tr>
	<td>Puerto del filtro: </td><td><input type="text" name="txt_puertof" id="txt_puertof" value="<?php print $filterport; ?>" /></td>
	</tr>
	<tr>
	<td>IP del proxy: </td><td><input type="text" name="txt_ipp" id="txt_ipp" value="<?php print $proxyip; ?>"/></td>
	</tr>
	<tr>
	<td>Puerto del proxy: </td><td><input type="text" name="txt_puertop" id="txt_puertop" value="<?php print $proxyport; ?>"/></td>
	</tr>
	<tr>
	<td>Grupos de filtrado: </td><td><input type="txt" name="txt_gf" id="txt_gf" value="<?php print $filtergroups; ?>"/></td>
	</tr>
	<tr>
	<td><button>Actualizar</button></td>
	</tr>
</table></form>
<hr>
<form name="regresar" action="../../index1.php">
		<button type="submit" style="background-color: #d9534f;">Regresar</button>
</form>
</body>
</html>
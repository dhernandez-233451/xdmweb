<?php
include_once("cabecera.php");
if(isset($_GET['ruta'])&&isset($_GET['archivo'])){
	$tipo=$_GET['tipo'];
	$url=$_GET['url'];
	$ruta=$_GET['ruta'];
	$archivo=$_GET['archivo'];
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Restauración de <?php print $tipo; ?></title>
</head>
<body>
<?php 
$ruta;
$archivo;

if(isset($_POST['hr'])){
	exec('sudo mv '.$ruta.$archivo.'.backup '.$ruta.$archivo);
	echo "<p><b>Restauración completada</b></p>";
	switch ($tipo) {
		case 'interfaces':
			exec('sudo /etc/init.d/networking restart',$rr);
			foreach ($rr as $lrr)print $lrr."</br>";
			break;
		case 'system':
			exec('sudo mv '.$ruta.'proxy.configurado.backup '.$ruta.'proxy.configurado');
			break;
		case 'named':
			exec('sudo service bind9 restart',$r);
			foreach ($r as $lr)print $lr."</br>";
			break;
		case 'netup':
			exec('sudo service xpertntc start',$xs);
			foreach ($xs as $lxs)print $lxs."</br>";
			break;
		case 'vpn':
			exec('sudo /etc/init.d/openvpn restart',$rvpn);
			foreach ($rvpn as $lv)print $lv."</br>";
			break;
		case 'gruposip':
			print shell_exec('sudo /etc/init.d/dansguardian stop')."</br>";
			print shell_exec('sudo /etc/init.d/dansguardian start')."</br>";
			print shell_exec('sudo /etc/init.d/dansguardian reload')."</br>";
			break;
		case 'dhcpd':
			exec('sudo /etc/init.d/dhcp3-server restart',$rdhcp);
			foreach ($rdhcp as $lrd)print $lrd."</br>";
			break;

		default:
			# code...
			break;
	}
}
else if(isset($_GET['ruta'])&&isset($_GET['archivo'])&&$_GET['ruta']!=""&&$_GET['archivo']!=""){
		$lista=shell_exec('sudo ls -l '.$ruta.' |grep "'.$archivo.'\.backup" |awk \'{print $7 " " $6}\'');
		if($lista!=""){
			print '<p>Configuración anterior de '.$tipo.': '.$lista."</br>";
			print '<form name="restaurar" method="post" action="">
			<button type="submit">Restaurar</button>
			<input type="hidden" id="hr" name="hr" value="hr">
			</form>';
		}
		else echo "No se encontraron configuraciones anteriores";	
	}
	else echo "Tipo de restauración no especificada";
?>
<form name="regresar" action="http://<?php print $url;?>">
		<button type="submit" style="background-color: #d9534f;">Regresar</button>
</form>
</body>
</html>
<?php
include_once("../cabecera.php");
#dir d rwx r-x r-x
#rch - rw- r-- r--
#
#$ruta="/wwnetworks/xadmon/pags/otras_config/ap/";#Acomodar ruta
$ruta="/etc/dhcp3/";
$archivo="dhcpd.conf";

#Recuperación############################
$tipo="dhcpd";							#
include_once("../lnk_rec.php");			#
#########################################
$parametros=array(array('host','Equipo'),array('ethernet','MAC'),array('fixed-address','IP'));

$escribir=false;
if(isset($_POST['lineas'])&&$_POST['lineas']!=0){
	exec('cat '.$ruta.$archivo,$contenido);
	$acumulador="";
	foreach ($contenido as $renglon) if(preg_match("/(host\s.+{\s.+\s.+\s.+\s;\s.+\s.+;\s})$/", $renglon))break;
		else $acumulador.=$renglon."\n";
	for ($i=0; $i <$_POST['lineas'] ; $i++) { 
		$cp=0;
		foreach ($parametros as $p) if(isset($_POST['txt_'.$i.'_'.$p[0]]))$cp++;
		if($cp==count($parametros)){
			if(!isset($_POST['chk_'.$i]))$acumulador.="#";
			$acumulador.=$parametros[0][0]." ".$_POST['txt_'.$i.'_'.$parametros[0][0]]." { hardware ".$parametros[1][0]." ".$_POST['txt_'.$i.'_'.$parametros[1][0]]." ; ".$parametros[2][0]." ".$_POST['txt_'.$i.'_'.$parametros[2][0]]."; }\n";
		}
	}
	$contenido=null;
	$escribir=true;
	#print $acumulador;
}
if($escribir){
	exec('sudo chmod u=rwx,g=rwx,o=rwx '.$ruta);
	exec('sudo chmod u=rw,g=rw,o=rw '.$ruta.$archivo);
	exec('sudo cat '.$ruta.$archivo.' > '.$ruta.$archivo.'.backup');
	exec('sudo rm '.$ruta.$archivo);
    $nuevo = fopen($ruta.$archivo, "a");
    fwrite($nuevo, trim($acumulador));
    fclose($nuevo);
	exec('sudo chmod u=rwx,g=rx,o=rx '.$ruta);
	exec('sudo chmod u=rw,g=r,o=r '.$ruta.$archivo);
	exec('sudo chown root:root '.$ruta.$archivo.'.backup');
	exec('sudo chown root:root '.$ruta.$archivo);

	echo "Actualización correcta</br>";
	echo "Reiniciando servicios...</br>";
	exec('sudo /etc/init.d/dhcp3-server restart',$rdhcp);
	foreach ($rdhcp as $lrd)print $lrd."</br>";
}
function mverificador($l,$comentario){
	$nombre=str_replace(" ", "_", $nombre);
	print ' Activar: <input type="checkbox" id="chk_'.$l.'" name ="chk_'.$l.'"';
	if(!$comentario)print ' checked="checked"';
	print '/>';
}
function metiqueta($nombre){
	print '<label>'.$nombre.': </label>';
}
function mcaja($l,$nombre,$valor){
	$nombre=str_replace(" ", "_", $nombre);
	print '<input type="text" id="txt_'.$l.'_'.$nombre.'" name="txt_'.$l.'_'.$nombre.'" value="'.$valor.'">';
}
/*function mbtnb($l,$nombre){
	print '<input type="button" id="btn_'.$l.'_'.$nombre.'" name="btn_'.$l.'_'.$nombre.'" value="Borrar" onClick="document.getElementById(\'txt_'.$l.'_'.$nombre.'\').value=\'\';document.getElementById(\'txt_'.$l.'_'.$nombre.'\').style.visibility=\'hidden\';document.getElementById(\'btn_'.$l.'_'.$nombre.'\').style.visibility=\'hidden\'">';
}*/

exec('cat '.$ruta.$archivo,$contenido);
$linea=0;
?>
<h2 align="center">Fijar MAC-IP a equipos</h2>
<h3>dhcpd.conf</h3>
<form id="frm_mi" name="frm_mi" method="post" action="">
<table border="0">
<?php
for ($i=0; $i < count($contenido); $i++) { 
	$renglon=$contenido[$i];
	if(preg_match("/(host\s.+{\s.+\s.+\s.+\s;\s.+\s.+;\s})$/", $renglon)){
		$comentario=false;
		if(preg_match("/^#/", $renglon)){
			$comentario=true;
			$renglon=str_replace("#", "", $renglon);
		}
		print '<tr><td>';
		mverificador($linea,$comentario);
		$palabras=explode(" ", $renglon);
		for ($j=0; $j <count($palabras) ; $j++) { 
			foreach ($parametros as $p) {
				if(trim($palabras[$j])==$p[0]){
					print '</td><td>';
					metiqueta($p[1]);
					print '</td><td>';
					$valor=str_replace(";", "", $palabras[$j+1]);
					mcaja($linea,$p[0],$valor);
					break;
				}
			}
		}
		print '</td></tr>';
		$linea++;
	}
}
print '<tr><td>Nuevo</td></tr><tr><td>';
mverificador($linea,false);
foreach ($parametros as $p) {
	print '</td><td>';
	metiqueta($p[1]);
	print '</td><td>';
	mcaja($linea,$p[0],"");
}
$linea++;
print '</td></tr>';
?>
</table>
<input type="hidden" id ="lineas" name="lineas" value="<?php print $linea ?>">
<button type="submit">Guardar</button>
</form>
<hr>
<form name="regresar" action="index.php">
		<button type="submit" style="background-color: #d9534f;">Regresar</button>
</form>
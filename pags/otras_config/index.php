<?php
	include_once("../../pags/cabecera.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Administración Xpert</title>
</head>
<body>

<h3 align="center">NETWORKING</h3>
<center>
	<table border="0" width="80%">
		<tr>
			<td align="center">
				<?php if(permiso($_SESSION["nivel"],24)) { print '<a href="a_interfaces.php"><h4>Interfaces de red</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],25)) { print '<a href="a_system.php"><h4>Archivo de configuración</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],26)) { print '<a href="a_netplugd.php"><h4>Netplug</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],27)) { print '<a href="a_dhcpd.php"><h4>DHCP</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],32)) { print '<a href="llamados.php?accion=1"><h4>Reactivar DNS Updater</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],34)) { print '<a href="llamados.php?accion=2"><h4>Conexiones</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],35)) { print '<a href="llamados.php?accion=4"><h4>Intentos de acceso</h4></a>';} ?>
			</td>
			<td> </td>
			<td align="center">
				<?php if(permiso($_SESSION["nivel"],28)) { print '<a href="a_namedcl.php"><h4>Filtrado por DNS</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],29)) { print '<a href="fipt/"><h4>Filtrado https</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],30)) { print '<a href="a_ipt.php"><h4>IPtables</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],31)) { print '<a href="llamados.php?accion=3"><h4>VPN</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],33)) { print '<h4>Configuración de modo</h4>
				<a href="a_cmodo.php?m=router">Router</a></br>
				<a href="a_cmodo.php?m=failover">FailOver</a></br>
				<a href="a_cmodo.php?m=loadbalancing">LoadBalancing</a></br>';} ?>
			</td>
		</tr>
		<tr>
			<td align="center">
				
			</td>
			<td></td>
			<td align="center">
				
			</td>
		</tr>
	</table>
</center>
<form name="regresar" action="../../index1.php">
		<button type="submit" style="background-color: #d9534f;">Regresar</button>
</form>
</body>
</html>

<?php
include_once("../../pags/cabecera.php");
#dir d rwx r-x r-x
#rch - rw- r-- r--
#
#$ruta="/wwnetworks/xadmon/pags/otras_config/ap/vpn/";
$ruta="/etc/openvpn/";
$archivo="";
$tipo="";


if(isset($_GET['t']))$tipo=$_GET['t'];
$parametros=array(0 => 'ifconfig',1 => 'route',2 => 'port',3 => 'log-append',4 => 'remote');
$parametrosxt=array(0 => 'IP virtual del tunel',1 => 'Rutas',2 => 'Puerto',3 => 'Archivo de registros',4 => 'IP del servidor VPN');

if(isset($_POST['tipo'])&&$_POST['tipo']!=""){
	$contenido="";

	$contenido.="#".$_POST['txt_0_0']."\n";
	if($_POST['tipo']=="server")$contenido.="local ".$_POST['txt_7_0']."\n#Tunel de datos\n";
	$contenido.="dev ".$_POST['txt_0_0']."\n";
	$contenido.="ifconfig ".$_POST['txt_1_0']." ".$_POST['txt_1_1']."\n";
	if($_POST['tipo']=="server")$contenido.="#up /etc/openvpn/up.sh\n";
	$contenido.="secret /etc/openvpn/default.key\n\n#Ruteos de Red\n";
	if($_POST['txt_2_0']!="")$contenido.="route ".$_POST['txt_2_0']." ".$_POST['txt_2_1']."\n";
	if($_POST['txt_3_0']!="")$contenido.="route ".$_POST['txt_3_0']." ".$_POST['txt_3_1']."\n";
	if($_POST['txt_4_0']!="")$contenido.="route ".$_POST['txt_4_0']." ".$_POST['txt_4_1']."\n";
	$contenido.="\nroute-delay 1\n";
	$contenido.="port ".$_POST['txt_5_0']."\n";
	$contenido.="comp-lzo\n";
	$contenido.="log-append ".$_POST['txt_6_0']."\n";
	if($_POST['tipo']=="server")$contenido.="#";
	$contenido.="resolv-retry infinite\n";
	if($_POST['tipo']=="cliente")$contenido.="remote ".$_POST['txt_7_0']."\n";
	$contenido.="up-delay\n";
	if($_POST['tipo']=="server")$contenido.="float\n#";
	$contenido.="nobind\n";
	if($_POST['tipo']=="server")$contenido.="#Parametros que no deben modificarse\n#user nobody\n#group nobody\n";
	$contenido.="ping 7\nping-restart 40\n";
	if($_POST['tipo']=="server")$contenido.="ping-timer-rem\n#persist-tun\n";
	$contenido.="persist-key\nverb 1\nmute 3\ntxqueuelen 120\nmssfix 1000";

	$archivo=$_POST['txt_0_0'].".conf";

	exec('sudo chmod u=rwx,g=rwx,o=rwx '.$ruta);
	$instruccion='echo "'.$contenido.'" > '.$ruta.$archivo;
	exec($instruccion);
	exec('sudo chmod u=rw,g=r,o=r '.$ruta.$archivo);
	exec('sudo chown root:root '.$ruta.$archivo);

	#echo "Modificación aplicada \n Reiniciando servicios...";
	#echo shell_exec('/etc/init.d/openvpn restrat')
	echo "Acción correcta";
	#print $contenido;
}

function mcaja($l,$i){
	print '<input type="text" id="txt_'.$l.'_'.$i.'" name="txt_'.$l.'_'.$i.'" value="">';
}
function mverificador($l,$comentario){
	print ' Activar: <input type="checkbox" id="chk_'.$l.'" name ="chk_'.$l.'"';
	if(!$comentario)print ' checked="checked"';
	print '/>';
}
$cajas=1;
$linea=0;
?>
<h2 align="center">VPN</h2>
<h3>Nuevo archivo <?php echo $tipo ?></h3>
<form id="frm_naovpn" name="frm_naovpn" method="post" action="">
<table border="0">
<?php
	print '<tr><td>Nombre del archivo: </td><td>';
	mcaja (0,0);
if($tipo=="cliente"){
	print '</td></tr><tr><td>'.$parametrosxt[0].': </td><td>';
	print 'IP host local: ';
	mcaja(1,0);
	print 'IP host destino: ';
	mcaja(1,1);
	print '</td></tr><tr><td>'.$parametrosxt[1].': </td><td>';
	print 'IP o segmento remoto: ';
	mcaja(2,0);
	print 'Mascara de red: ';
	mcaja(2,1);
	print '</br>';
	print 'IP o segmento remoto: ';
	mcaja(3,0);
	print 'Mascara de red: ';
	mcaja(3,1);
	print '</br>';
	print 'IP o segmento remoto: ';
	mcaja(4,0);
	print 'Mascara de red: ';
	mcaja(4,1);
	print '</td></tr><tr><td>'.$parametrosxt[2].': </td><td>';
	mcaja(5,0);
	print '</td></tr><tr><td>'.$parametrosxt[3].': </td><td>';
	mcaja(6,0);
	print '</td></tr><tr><td>'.$parametrosxt[4].': </td><td>';
	mcaja(7,0);
}
else if($tipo == "server"){
	print '</td></tr><tr><td> local: </td><td>';
	mcaja(7,0);
	print '</td></tr><tr><td>'.$parametrosxt[0].': </td><td>';
	print 'IP host local: ';
	mcaja(1,0);
	print 'IP host destino: ';
	mcaja(1,1);
	print '</td></tr><tr><td>'.$parametrosxt[1].': </td><td>';
	print 'IP o segmento remoto: ';
	mcaja(2,0);
	print 'Mascara de red: ';
	mcaja(2,1);
	print '</br>';
	print 'IP o segmento remoto: ';
	mcaja(3,0);
	print 'Mascara de red: ';
	mcaja(3,1);
	print '</br>';
	print 'IP o segmento remoto: ';
	mcaja(4,0);
	print 'Mascara de red: ';
	mcaja(4,1);
	print '</td></tr><tr><td>'.$parametrosxt[2].': </td><td>';
	mcaja(5,0);
	print '</td></tr><tr><td>'.$parametrosxt[3].': </td><td>';
	mcaja(6,0);
}
print '</td></tr>';
?>
</table>
<input type="hidden" id ="tipo" name="tipo" value="<?php print $tipo ?>">
<button type="submit">Guardar</button>
</form>
<hr>
<form name="regresar" action="index.php">
		<button type="submit" style="background-color: #d9534f;">Regresar</button>
</form>
<?php 
include_once("../../pags/cabecera.php");
#dir d rwx r-x r-x	root root
#rch - rw- r-- r--	root root
#
#$ruta="/wwnetworks/xadmon/pags/otras_config/ap/";
$ruta="/etc/network/";

#Recuperación############################
$tipo="interfaces";						#
$archivo="interfaces";					#
include_once("../../pags/lnk_rec.php"); #
#########################################
if(isset($_POST['cantidad'])&&$_POST['cantidad']!=0){
	$contenido="";
	for ($i=1; $i <=$_POST['cantidad']; $i++) {
		$contenido.="auto ";
		$contenido.=$_POST['h_'.$i]."\n\t";
		$contenido.="iface ".$_POST['h_'.$i]." inet ".$_POST['slinet_'.$i]."\n\t";
		if($_POST['slinet_'.$i]=="static"){
			if(isset($_POST['txt_'.$i.'_address'])&&$_POST['txt_'.$i.'_address']!="")$contenido.="address ".$_POST['txt_'.$i.'_address']."\n\t";
			if(isset($_POST['txt_'.$i.'_netmask'])&&$_POST['txt_'.$i.'_netmask']!="")$contenido.="netmask ".$_POST['txt_'.$i.'_netmask']."\n\t";
			if(isset($_POST['txt_'.$i.'_broadcast'])&&$_POST['txt_'.$i.'_broadcast']!="")$contenido.="broadcast ".$_POST['txt_'.$i.'_broadcast']."\n\t";
			if(isset($_POST['txt_'.$i.'_network'])&&$_POST['txt_'.$i.'_network']!="")$contenido.="network ".$_POST['txt_'.$i.'_network']."\n\t";
			if(isset($_POST['txt_'.$i.'_gateway'])&&$_POST['txt_'.$i.'_gateway']!="")$contenido.="gateway ".$_POST['txt_'.$i.'_gateway']."\n\t";
		}
		$contenido.="\n";
	}
	exec('sudo chmod u=rwx,g=rwx,o=rwx '.$ruta);
	exec('cat '.$ruta.'interfaces > '.$ruta.'interfaces.backup');
	$instruccion='echo "'.$contenido.'" > '.$ruta.'interfaces';
	exec($instruccion);
	exec('sudo chmod u=rw,g=rw,o=rw '.$ruta.'interfaces.backup');
	exec('sudo chmod u=rw,g=rw,o=rw '.$ruta.'interfaces');
	exec('sudo chown root:root '.$ruta.'interfaces.backup');
	exec('sudo chown root:root '.$ruta.'interfaces');

	echo "Actualización correcta</br>";
	echo "Reiniciando servicios...</br>";
	exec('sudo /etc/init.d/networking restart',$rr);
	foreach ($rr as $lrr)print $lrr."</br>";
	/*con ajax print "<script language=\"JavaScript\">  
	pre();
	function pre(){
    	if (confirm('Reiniciar servicios de red')){ 
       	location.href =\"/pags/consola_admon/acciones.php?accion=12\";
    	}
	}
	</script>";*/

}
?>
<h2 align="center">Configuración de red</h2>
<h3>Intefaces</h3>
<form id="frm_if" name="frm_if" method="post" action="">
<?php
#system.conf, xpert type mode
$xpertmode=false;
exec('cat /wwnetworks/conf/system.conf',$xsysc);
foreach ($xsysc as $xs)if(preg_match("/^(MODE=)/", $xs)){
		$xs=str_replace(array(" ","\t","#"), "", $xs);
		$xlinea=explode("=", $xs);

		if($xlinea[1]=="NONE"){
			$xpertmode=true;
			break;
		}
	}
$existentes;
$ifs=0;
$contenido=shell_exec('cat '.$ruta.'interfaces');
function inet($if,$in,$ifs){
	$otro="static";
	if($in=="static")$otro="dhcp";
	print '<label>inet</label><select id="slinet_'.$ifs.'" name="slinet_'.$ifs.'">
	<option value="'.$in.'" selected>'.$in.'</option>
	<option value="'.$otro.'">'.$otro.'</option>
	</select>
	<input type="hidden" id="h_'.$ifs.'" name="h_'.$ifs.'" value="'.$if.'">
	</br>';
}
function atributos($nom,$conf,$ifs){
	print '<label>'.$nom.'</label><input type="text" id="txt_'.$ifs.'_'.$nom.'" name="txt_'.$ifs.'_'.$nom.'" value="'.$conf.'"></br>';
}
function faltantes($exist,$ifs,$w1){
	$todos= array(0 => 'address',1 => 'netmask', 2=> 'broadcast', 3=> 'network'/*, 4=> 'gateway'*/);
	if($w1)$todos[]='gateway';
	foreach ($todos as $t){
		$poner=true;
		foreach ($exist as $ex)if($t==$ex)$poner=false;
		if($poner)atributos($t,"",$ifs);
	} 
}
#echo $contenido."</br>";
$xcont=str_replace(array(" ","\n","\t","\r","\r\n")," ", $contenido);
$palabras=explode(" ", $xcont);
$xif="";
for ($i=1;$i<count($palabras);$i++) {
	if(preg_match("/^(eth[0-9])$/", $palabras[$i])&&$palabras[$i-1]=="auto"){
		$ifs++;
		for($j=$i;$j<count($palabras);$j++){
			if($palabras[$j]=="inet"){
				print '<p>';
				echo $palabras[$i]."</br>";
				$j++;
				if($palabras[$j]=="static"){
					$existentes;
					$xif=$palabras[$j-2];
					inet($palabras[$j-2],$palabras[$j],$ifs);
					for($k=$j;$k<count($palabras);$k++){
						if($palabras[$k]=="address"||$palabras[$k]=="netmask"||$palabras[$k]=="broadcast"||$palabras[$k]=="network"||$palabras[$k]=="gateway"){
							atributos($palabras[$k],$palabras[$k+1],$ifs);
							$existentes[]=$palabras[$k];
							$k++;
						}
						if(preg_match("/^(eth[0-9])$/", $palabras[$k+1])&&$palabras[$k]=="auto"){
							if(count($existentes)==0)$existentes[]="";
							if($xif=="eth1")faltantes($existentes,$ifs,$xpertmode);
							else faltantes($existentes,$ifs,false);
							$existentes=null;
							break;
						}
						else if($k>(count($palabras)-5)){
							if(count($existentes)==0)$existentes[]="";
							if($xif=="eth1")faltantes($existentes,$ifs,true);
							else faltantes($existentes,$ifs,false);
							$existentes=null;
							break;
						}
					}
					$j=$k;
				}
				else if($palabras[$j]=="dhcp"){
					inet($palabras[$j-2],$palabras[$j],$ifs);
					//faltantes(array(""),$ifs);
				}
				print '</p>';
				break;
			}
			$i=$j;
		}
	}	
}
?>
<input type="hidden" id ="cantidad" name="cantidad" value="<?php print $ifs ?>">
<button type="submit">Guardar</button>
</form>
<hr>
<form name="regresar" action="index.php">
		<button type="submit" style="background-color: #d9534f;">Regresar</button>
</form>
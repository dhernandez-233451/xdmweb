<?php
include_once("../cabecera.php");
#dir d rwx r-x r-x
#rch - rw- r-- r--
#
#$ruta="/wwnetworks/xadmon/pags/otras_config/ap/";#Acomodar ruta
$ruta="/etc/dhcp3/";
$archivo="dhcpd.conf";

#Recuperación############################
$tipo="dhcpd";							#
include_once("../lnk_rec.php");			#
#########################################
$parametros=array(array('domain-name','Nombre de dominio'),array('local-proxy-config','Proxy local'),array('wpad','WPAD'),array('domain-name-servers','Servidor DNS'),array('subnet-mask','Mascara de subred'),array('broadcast-address','Dirección broadcast'),array('routers','Routers'),array('range','Rango'));
$parametros0=array(array('subnet','Subred'),array('netmask','Mascara de red'));

$escribir=false;
if(isset($_POST['lineas'])&&$_POST['lineas']!=0){
	exec('cat '.$ruta.$archivo,$contenido);
	$acumulador="";
	$r=0;
	for($r;$r<count($contenido);$r++)if(preg_match("/^(subnet)/", $contenido[$r])){
			$partes=explode(" ", $contenido[$r]);
			for ($j=0; $j < count($partes); $j++) { 
				foreach ($parametros0 as $pa0) {
					if(trim($partes[$j])==$pa0[0]){
						$acumulador.=$pa0[0]." ";
						for ($i=0; $i <$_POST['lineas']; $i++) { 
							for ($k=0; $k <$_POST['cajas'] ; $k++) { 
								if(isset($_POST['txt_'.$i.'_'.$pa0[0].'_'.$k])){
									$acumulador.=$_POST['txt_'.$i.'_'.$pa0[0].'_'.$k]." ";
									break;
								}
							}
							
						}
						$j++;
						break;
					}
				}
			}
			$acumulador.="{\n";
			break;
		}
		else $acumulador.=$renglon."\n";
	for ($j=0; $j <count($parametros) ; $j++) { 
		if($j<7)$acumulador.="\toption ".$parametros[$j][0]." ";
		else $acumulador.="\t".$parametros[$j][0]." ";
		for ($i=0; $i <$_POST['lineas']; $i++) { 
			$encontrado=false;
			for ($k=0; $k <=$_POST['cajas'] ; $k++) { 
				if(isset($_POST['txt_'.$i.'_'.$parametros[$j][0].'_'.$k])){
					$encontrado=true;
					if(preg_match("/\\\\/", $_POST['txt_'.$i.'_'.$parametros[$j][0].'_'.$k]))$caja=str_replace("\\\\", "\\", $_POST['txt_'.$i.'_'.$parametros[$j][0].'_'.$k]);
					else $caja=$_POST['txt_'.$i.'_'.$parametros[$j][0].'_'.$k];
					if($j<3)$acumulador.="\"".$caja."\" ";
					else $acumulador.=$caja." ";
				}
				if($encontrado&&$k==$_POST['cajas']){
					$acumulador=trim($acumulador).";\n";
				}
			}
		}
	}
	$acumulador.="\t}\n";
	$r=$r+$_POST['lineas'];
	for ($r; $r <count($contenido) ; $r++) $acumulador.=$contenido[$r]."\n";
	$contenido=null;
	$escribir=true;
	#print $acumulador;
}
if($escribir){
	exec('sudo chmod u=rwx,g=rwx,o=rwx '.$ruta);
	exec('sudo chmod u=rw,g=rw,o=rw '.$ruta.$archivo);
	exec('sudo cat '.$ruta.$archivo.' > '.$ruta.$archivo.'.backup');
	exec('sudo rm '.$ruta.$archivo);
    $nuevo = fopen($ruta.$archivo, "a");
    fwrite($nuevo, trim($acumulador));
    fclose($nuevo);
	exec('sudo chmod u=rwx,g=rx,o=rx '.$ruta);
	exec('sudo chmod u=rw,g=r,o=r '.$ruta.$archivo);
	exec('sudo chown root:root '.$ruta.$archivo.'.backup');
	exec('sudo chown root:root '.$ruta.$archivo);

	echo "Actualización correcta</br>";
	echo "Reiniciando servicios...</br>";
	exec('sudo /etc/init.d/dhcp3-server restart',$rdhcp);
	foreach ($rdhcp as $lrd)print $lrd."</br>";
}

function metiqueta($nombre){
	print '<label>'.$nombre.': </label>';
}
function mcaja($l,$nombre,$valor,$i){
	$nombre=str_replace(" ", "_", $nombre);
	print '<input type="text" id="txt_'.$l.'_'.$nombre.'_'.$i.'" name="txt_'.$l.'_'.$nombre.'_'.$i.'" value="'.$valor.'">';
}

exec('cat '.$ruta.$archivo,$contenido);
$cajas=0;
$c=0;
$linea=0;
?>
<h2 align="center">Configuración DHCP</h2>
<h3>dhcpd.conf</h3>
<p><a href="a_dhcpd_mi.php"><i>Fijar MAC-IP a equipos</i></a></p>
<form id="frm_d" name="frm_d" method="post" action="">
<table border="0">
<?php
for ($i=0; $i < count($contenido); $i++) { 
	if(preg_match("/^(subnet)/", $contenido[$i])){
		$palabras=explode(" ", $contenido[$i]);
		for ($j=0; $j <count($palabras) ; $j++) { 
			foreach ($parametros0 as $pa0) {
				if(trim($palabras[$j])==$pa0[0]){
					print '<tr><td>';
					metiqueta($pa0[1]);
					print '</td><td>';
					mcaja($linea++,$pa0[0],$palabras[$j+1],$c);
					print '</td></tr>';
					break;
				}
			}
		}
		for ($i=$i+1; $i <count($contenido) ; $i++) { 
			foreach ($parametros as $p) {
				if(preg_match("/(".$p[0]."\s)/", $contenido[$i])){
					$palabras=explode(" ", $contenido[$i]);
					$v=$palabras[count($palabras)-1];
					$v=str_replace(array(" ",";","\""), "", $v);
					if(preg_match("/(\d\s\d)/", $contenido[$i])){
						$v0=$palabras[count($palabras)-2];
						$v0=str_replace(array(" ",";","\""), "", $v0);
						print '<tr><td>';
						metiqueta($p[1]);
						print '</td><td>';
						mcaja($linea,$p[0],$v0,$c++);
						mcaja($linea++,$p[0],$v,$c);
						print '</td></tr>';
					}
					else{
						print '<tr><td>';
						metiqueta($p[1]);
						print '</td><td>';
						mcaja($linea++,$p[0],$v,$c);
						print '</td></tr>';
					}
					if($c>$cajas)$cajas=$c;
					$c=0;
					break;
				}
			}
		}
		break;
	}
}
?>
</table>
<input type="hidden" id ="lineas" name="lineas" value="<?php print $linea ?>">
<input type="hidden" id ="cajas" name="cajas" value="<?php print $cajas ?>">
<button type="submit">Guardar</button>
</form>
<hr>
<form name="regresar" action="index.php">
		<button type="submit" style="background-color: #d9534f;">Regresar</button>
</form>
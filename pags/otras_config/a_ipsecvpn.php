<?php
include_once("../../pags/cabecera.php");
$ruta="/etc/";#"/wwnetworks/xadmon/pags/otras_config/ap/vpn/";
$archivo="ipsec.conf";


$parametros=array(0 => 'config setup',1 => 'include',2 => 'conn');
$parametros_asignacion=array(0 => 'plutodebug',1 => 'plutoopts',2 => 'nat_traversal',3 => 'virtual_private',4 => 'oe',5 => 'protostack',6 => 'left',7 => 'leftsubnet',8 => 'leftnexthop',9 => 'right',10 => 'rightsubnet',11 => 'rightnexthop',12 => 'auto');

if(isset($_POST['lineas'])&&$_POST['lineas']!=0){
	$contenido="";
	for ($i=0; $i < $_POST['lineas']; $i++) { 
		if(isset($_POST['hdn_'.$i])){
			$nc=str_replace('_cmll_', '\"', $_POST['hdn_'.$i]);
			$contenido.=$nc;
		}
		for ($j=0; $j < count($parametros); $j++) { 
			if(isset($_POST['chk_'.$i.'_'.$parametros[$j]]))$contenido.="#";
				for ($k=0; $k < $_POST['cajas']; $k++)if(isset($_POST['txt_'.$i.'_'.$parametros[$j].'_'.$k])){
					if($k==0)$contenido.=$_POST['txt_'.$i.'_'.$parametros[$j].'_'.$k];
					else if($k==1) $contenido.= $parametros[$j]." ".$_POST['txt_'.$i.'_'.$parametros[$j].'_'.$k];
					else $contenido.=" ".$_POST['txt_'.$i.'_'.$parametros[$j].'_'.$k];
				}
		}
		for ($j=0; $j < count($parametros_asignacion); $j++) { 
			if(isset($_POST['chk_'.$i.'_'.$parametros_asignacion[$j]]))$contenido.="#";
			if($j<2){
				for ($k=0; $k < $_POST['cajas']; $k++)if(isset($_POST['txt_'.$i.'_'.$parametros_asignacion[$j].'_'.$k])){
					if($k==0)$contenido.='\"'.$_POST['txt_'.$i.'_'.$parametros_asignacion[$j].'_'.$k].'\"';
					else if($k==1)$contenido.= $parametros_asignacion[$j].'=\"'.$_POST['txt_'.$i.'_'.$parametros_asignacion[$j].'_'.$k].'\"';
					else $contenido.='\"'.$_POST['txt_'.$i.'_'.$parametros_asignacion[$j].'_'.$k].'\"';
				}
			}
			else{
				for ($k=0; $k < $_POST['cajas']; $k++)if(isset($_POST['txt_'.$i.'_'.$parametros_asignacion[$j].'_'.$k])){
					if($k==0)$contenido.=$_POST['txt_'.$i.'_'.$parametros_asignacion[$j].'_'.$k];
					else if($k==1) $contenido.= $parametros_asignacion[$j]."=".$_POST['txt_'.$i.'_'.$parametros_asignacion[$j].'_'.$k];
					else $contenido.=" ".$_POST['txt_'.$i.'_'.$parametros_asignacion[$j].'_'.$k];
				}
			}
		}
		$contenido.="\n";
	}
	exec('cat '.$ruta.$archivo.' > '.$ruta.$archivo.'.backup');
	$instruccion='echo "'.$contenido.'" > '.$ruta.$archivo;
	exec('sudo chmod -R u=rwx,g=rwx,o=rwx '.$ruta);
	exec($instruccion);
	echo "Modificación aplicada \n Reiniciando servicios...";
	#código para reiniciar servicios
	echo "Actualización correcta";
	#print $contenido;
	$contenido="";
}

exec('cat '.$ruta.$archivo,$contenido);

function metiqueta($nombre){
	print '<label>'.$nombre.'</label>';
}
function mcaja($l,$nombre,$valor,$i){
	print '<input type="text" id="txt_'.$l.'_'.$nombre.'_'.$i.'" name="txt_'.$l.'_'.$nombre.'_'.$i.'" value="'.$valor.'">';
}
function mverificador($l,$nombre,$comentario){
	print ' Comentar: <input type="checkbox" id="chk_'.$l.'_'.$nombre.'" name ="chk_'.$l.'_'.$nombre.'"';
	if($comentario)print ' checked="checked"';
	print '/>';
}
function mnota($l,$cadena,$xc){
	echo $cadena;
	print '<input type="hidden" id="hdn_'.$l.'" name="hdn_'.$l.'" value="'.$xc.'" >';
}
$cajas=1;
$linea=0;
?>
<h2 align="center">VPN</h2>
<h3>IPsec</h3>
<form id="frm_vpnipsec" name="frm_vpnipsec" method="post" action="">
<?php
foreach ($contenido as $lc) {
	print '<p>';
	$renglon=str_replace("\t", '', $lc);
	$comentario=false;
	if(preg_match("/\#/", $lc)){
		$renglon=str_replace('# ','#',$lc);
		$renglon=str_replace("\t", '', $renglon);
		$paux=explode("#", $renglon);
		$renglon=$paux[count($paux)-1];
		$comentario=true;
	}
	$comillas=false;
	if(preg_match("/=/", $renglon)){
		if(preg_match("/\"/", $renglon)){
			$comillas=true;
			$renglon=str_replace('"','',$renglon);
		}
		$palabras=explode("=", $renglon);
	}
	else $palabras=explode(" ", $renglon);
	if($cajas<count($palabras))$cajas=count($palabras);
	$palabra=$palabras[0];
	$nota=true;
	for ($i=0; $i <count($parametros) ; $i++) if($palabra==$parametros[$i]){
			$nota=false;
			mverificador($linea,$palabra,$comentario);
			if(count($palabras)>1){
				metiqueta($palabra);
				for ($j=1; $j <count($palabras) ; $j++) mcaja($linea,$palabra,$palabras[$j],$j);
			}
			else mcaja($linea,$palabra,$palabra,0);
			break;
		}
	for ($i=0; $i <count($parametros_asignacion) ; $i++) if($palabra==$parametros_asignacion[$i]){
			$nota=false;
			mverificador($linea,$palabra,$comentario);
			if(count($palabras)>1){
				metiqueta($palabra);
				for ($j=1; $j <count($palabras) ; $j++) mcaja($linea,$palabra,$palabras[$j],$j);
			}
			else mcaja($linea,$palabra,$palabra,0);
			break;
		}
	if($nota){
		$xlc=str_replace('"', '_cmll_', $lc);
		mnota($linea,$lc,$xlc);
	} 
	print '</p>';
	$linea++;
}
?>
<input type="hidden" id ="lineas" name="lineas" value="<?php print $linea ?>">
<input type="hidden" id ="cajas" name="cajas" value="<?php print $cajas ?>">
<button type="submit">Guardar</button>
</form>
<hr>
<form name="regresar" action="index.php">
		<button type="submit" style="background-color: #d9534f;">Regresar</button>
</form>
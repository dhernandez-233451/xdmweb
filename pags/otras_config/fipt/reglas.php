<?php
include_once("../../../pags/cabecera.php");
#dir d rwx rwx rwx
#rch - rwx rwx rwx
#
$contenido=null;
$escribir=false;
$acumulador="";
$grupo="";
$ruta="";
$archivo="";
if(isset($_GET['g'])&&$_GET['g']!=""){
	$grupo=$_GET['g'];
	#$ruta="/wwnetworks/xadmon/pags/otras_config/ipsec/ap/";#acomodar rutas#"/wwnetworks/scripts/";
	$ruta="/wwnetworks/scripts/";
	$archivo="netup.sh";
}
if(isset($_POST['hap'])){
	exec('sudo cat '.$ruta.$archivo,$contenido);
	if($grupo!="standard"){
		foreach ($contenido as $linea) {
			$acumulador.=$linea."\n";
			if(preg_match("/(---por)/", $linea)){
				if($_POST['rb_ap']==1)$parametro="DROP";
				if($_POST['rb_ap']==0)$parametro="ACCEPT";
				$acumulador.="/sbin/iptables -t filter -A FORWARD -i eth0 -m set --match-set ".$grupo." src -m string --algo bm --string \"".$_POST['txt_np']."\" -j ".$parametro." && echo \"set y A ok\"\n";
			}
		}
	}
	else{
		if($_POST['rb_ap']==1){
			foreach ($contenido as $linea) {
				$acumulador.=$linea."\n";
				if(preg_match("/(---general \(REJECT\))/", $linea))$acumulador.="/sbin/iptables -t filter -A FORWARD -i eth0 -m string --algo bm --string \"".$_POST['txt_np']."\" -j REJECT --reject-with icmp-proto-unreachable && echo \"string n D ok\"\n";
			}
		}
		else{
			foreach ($contenido as $linea) {
				$acumulador.=$linea."\n";
				if(preg_match("/(---general \(ACCEPT\))/", $linea))$acumulador.="/sbin/iptables -t nat -I PREROUTING -d ".$_POST['txt_np']." -j ACCEPT\n";
			}
		}
	}
	$escribir=true;
}
else if(isset($_POST['hacp'])){
	$permitidos=null;
	$denegados=null;
	exec('sudo cat '.$ruta.$archivo,$contenido);
	if($grupo!="standard"){
		for ($i=0; $i <$_POST['hdn_lineas']; $i++) {
			if(!isset($_POST['chk_'.$i]))if(isset($_POST['htxt_'.$i])){
				if($_POST['rb_'.$i]==0)$permitidos[]="/sbin/iptables -t filter -A FORWARD -i eth0 -m set --match-set ".$grupo." src -m string --algo bm --string \"".$_POST['htxt_'.$i]."\" -j ACCEPT && echo \"set y A ok\"";
				else if($_POST['rb_'.$i]==1)$denegados[]="/sbin/iptables -t filter -A FORWARD -i eth0 -m set --match-set ".$grupo." src -m string --algo bm --string \"".$_POST['htxt_'.$i]."\" -j DROP && echo \"set y A ok\"";
				
			}
		}
		for($j=0;$j<count($contenido);$j++) {
			$acumulador.=$contenido[$j]."\n";
			if(preg_match("/(---por)/", $contenido[$j])){
				if(count($permitidos)>0)foreach ($permitidos as $p)$acumulador.=$p."\n";
				if(count($denegados)>0)foreach ($denegados as $d)$acumulador.=$d."\n";
				for ($k=0; $k < $_POST['hdn_lineas']; $k++) if(isset($_POST['hdn_'.$k])){
					$instruccion=str_replace("_cmll_", "\"", $_POST['hdn_'.$k]);
					$acumulador.=$instruccion."\n";
				}
				$j++;
				for ($j; $j <count($contenido); $j++) if(preg_match("/(#){3,}/", $contenido[$j]))break;
				$j--;
			}
		}
	}
	else
	{
		for ($i=0; $i <$_POST['hdn_lineas']; $i++) { 
			if(!isset($_POST['chk_'.$i]))if(isset($_POST['htxt_'.$i])){
				if($_POST['rb_'.$i]==0)$permitidos[]="/sbin/iptables -t nat -I PREROUTING -d ".$_POST['htxt_'.$i]." -j ACCEPT";
				else if($_POST['rb_'.$i]==1)$denegados[]="/sbin/iptables -t filter -A FORWARD -i eth0 -m string --algo bm --string \"".$_POST['htxt_'.$i]."\" -j REJECT --reject-with icmp-proto-unreachable && echo \"string n D ok\"";
				
			}
		}
		$lista=1;
		for($j=0;$j<count($contenido);$j++) {
			$acumulador.=$contenido[$j]."\n";
			if(preg_match("/(---general)/", $contenido[$j])){
				if($lista==2){
					if(count($permitidos)>0)foreach ($permitidos as $p)$acumulador.=$p."\n";
				}
				else {
					if(count($denegados)>0)foreach ($denegados as $d)$acumulador.=$d."\n";
					$lista=2;
				}
				$j++;
				for ($j; $j <count($contenido); $j++) if(preg_match("/(#){3,}/", $contenido[$j]))break;
				$j--;
			}
		}
	}
	$escribir=true;
}
if($escribir){
	#echo $acumulador;
	exec('sudo chmod -R u=rwx,g=rwx,o=rwx '.$ruta);
	exec('sudo cat '.$ruta.$archivo.' > '.$ruta.$archivo.'.backup');
	exec('sudo rm '.$ruta.$archivo);
    $nuevo = fopen($ruta.$archivo, "a");
    fwrite($nuevo, trim($acumulador));
    fclose($nuevo);
	exec('sudo chmod -R u=rwx,g=rwx,o=rwx '.$ruta);
	exec('sudo chown root:root '.$ruta.$archivo.'.backup');
	exec('sudo chown root:root '.$ruta.$archivo);

	echo "Actualización correcta</br>";
	echo "Reiniciando servicios...</br>";
	exec('sudo .'.$ruta.$archivo,$xs0);
	foreach ($xs0 as $lxs0)print $lxs0."</br>";
	exec('sudo service xpertntc start',$xs);
	foreach ($xs as $lxs)print $lxs."</br>";
}
function mverificador($nombre){
	$nombre=str_replace(array(" ","."), "", $nombre);
	print 'Quitar:<input type="checkbox" id="chk_'.$nombre.'" name ="chk_'.$nombre.'"/>';
}
function mopcion($nombre,$valor){
	$nombre=str_replace(array(" ","."), "", $nombre);
	$valor=str_replace("\"", "_cmll_", $valor);
	print '<input type="radio" id="rb_'.$nombre.'" name="rb_'.$nombre.'" value="0" ';
	if($valor==0)print 'checked';
	print '>Permitir
	<input type="radio" id="rb_'.$nombre.'" name="rb_'.$nombre.'" value="1" ';
	if($valor==1)print 'checked';
	print '>Denegar';
}
function mnota($nombre,$valor){
	$nombre=str_replace(array(" ","."), "", $nombre);
	$valor=str_replace("\"", "_cmll_", $valor);
	print '<input type="hidden" id="hdn_'.$nombre.'" name="hdn_'.$nombre.'" value="'.$valor.'" >';
}
function mncaja($nombre,$valor){
	$nombre=str_replace(array(" ","."), "", $nombre);
	$valor=str_replace("\"", "_cmll_", $valor);
	print '<input type="hidden" id="htxt_'.$nombre.'" name="htxt_'.$nombre.'" value="'.$valor.'" >'.$valor;
}
?>
<h2 align="center">Modificaciones personalizadas del firewall</h2>
<h3>Filtrado para <b><?php print $grupo; ?></b></h3>
<form id="frm_ap" name="frm_ap" method="post" action="">
Nueva palabra: <input type="text" name="txt_np" id="txt_np">
<?php mopcion("ap",0); ?>
<input type="hidden" name="hap" id="hap" value="agregar">
<button type="submit">Guardar</button>
</form>
<form id="frm_acp" name="frm_acp" method="post" action="">
<table>
	<?php
	$lineas=0;
	exec('sudo cat '.$ruta.$archivo,$renglones);
	if($grupo!="standard"){
		for ($i=0; $i <count($renglones) ; $i++) { 
			if(preg_match("/(---por)/", $renglones[$i])){
				$i++;
				for ($i; $i <count($renglones) ; $i++) { 
					if(preg_match("/(^\/sbin\/iptables)/", $renglones[$i])){
						if(preg_match("/(".$grupo.")/", $renglones[$i])){
							print '<tr><td>';
							$partes=explode(" ", $renglones[$i]);
							mverificador($lineas);
							print '</td><td>';
							$palabra=str_replace("\"", "", $partes[17]);
							mncaja($lineas,$palabra);
							print '</td><td>';
							if(trim($partes[19])=='ACCEPT')mopcion($lineas,0);
							else mopcion($lineas,1);
							print '<td></tr>';
						}
						else mnota($lineas,$renglones[$i]);
						$lineas++;
					}
					else break;
				}
				break;
			}
		}
	}
	else {
		for ($i=0; $i <count($renglones) ; $i++) { 
			if(preg_match("/(---general \(REJECT\))/", $renglones[$i])){
				$i++;
				for ($i; $i <count($renglones) ; $i++) { 
					if(preg_match("/(^\/sbin\/iptables)/", $renglones[$i])){
						print '<tr><td>';
						$partes=explode(" ", $renglones[$i]);
						mverificador($lineas);
						print '</td><td>';
						$palabra=str_replace("\"", "", $partes[12]);
						mncaja($lineas,$palabra);
						print '</td><td>';
						if(trim($partes[14])=='REJECT')mopcion($lineas,1);
						else mopcion($lineas,0);
						print '<td></tr>';
					}
					else break;
					$lineas++;
				}
			}
			if(preg_match("/(---general \(ACCEPT\))/", $renglones[$i])){
				$i++;
				for ($i; $i <count($renglones) ; $i++) { 
					if(preg_match("/(^\/sbin\/iptables)/", $renglones[$i])){
						print '<tr><td>';
						$partes=explode(" ", $renglones[$i]);
						mverificador($lineas);
						print '</td><td>';
						$palabra=str_replace("\"", "", $partes[6]);
						mncaja($lineas,$palabra);
						print '</td><td>';
						if(trim($partes[8])=='ACCEPT')mopcion($lineas,0);
						else mopcion($lineas,1);
						print '<td></tr>';
					}
					else break;
					$lineas++;
				}
			}
		}
	}
	mnota("lineas",$lineas);
	?>
	<tr><td></td><td></td><td></td></tr>
</table>
<input type="hidden" name="hacp" id="hacp" value="actualizar">
<button type="submit">Actualizar</button>
</form>
<hr>
<form name="regresar" action="index.php">
		<button type="submit" style="background-color: #d9534f;">Regresar</button>
</form>
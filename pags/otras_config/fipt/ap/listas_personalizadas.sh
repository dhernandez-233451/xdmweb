#!/bin/bash
echo "Creando lista dropbox_users"
/usr/sbin/ipset --create dropbox_users iphash
echo "Creando lista hotmail_users"
/usr/sbin/ipset --create hotmail_users iphash
echo "Limpiando listas"
/usr/sbin/ipset --flush dropbox_users
/usr/sbin/ipset --flush hotmail_users
echo "Alimentando lista dropbox_users"
ipset -A dropbox_users 192.168.254.200
ipset -A dropbox_users 192.168.254.201
echo "Alimentando lista hotmail_users"
ipset -A hotmail_users 192.168.254.100
ipset -A hotmail_users 192.168.254.110
ipset -A hotmail_users 192.168.254.120
#echo "Eliminando Usuarios"#este no
#ipset -D dropbox_users 192.168.254.202
echo "Salvando listas"
ipset --save dropbox_users
ipset --save hotmail_users
echo " ------- Listas creadas exitosamente -------"

<?php
include_once("../../../pags/cabecera.php");
#dir d rwx rwx rwx
#rch - rwx rwx rwx
#
$contenido=null;
$escribir=false;
$acumulador="";
$grupo="";
$ruta="";
$archivo="";
if(isset($_GET['g'])&&$_GET['g']!=""){
	$grupo=$_GET['g'];
	#$ruta="/wwnetworks/xadmon/pags/otras_config/ipsec/ap/";#acomodar rutas#"/wwnetworks/scripts/";
	$ruta="/wwnetworks/scripts/";
	$archivo="listas_personalizadas.sh";
}
if(isset($_POST['haip'])){
	exec('sudo cat '.$ruta.$archivo,$contenido);
	foreach ($contenido as $linea) {
		$acumulador.=$linea."\n";
		if(preg_match("/(Alimentando lista ".$grupo.")/", $linea))$acumulador.='ipset -A '.$grupo.' '.$_POST['txt_nip']."\n";
	}
	$escribir=true;
}
else if(isset($_POST['heip'])){
	exec('sudo cat '.$ruta.$archivo,$contenido);
	foreach ($contenido as $linea) {
		if(preg_match("/(ipset -A ".$grupo.")/", $linea)){
			$partes=explode(" ", $linea);
			$ip=str_replace(".", "", trim($partes[3]));
			if(!isset($_POST['chk_'.$grupo.$ip]))$acumulador.=$linea."\n";
		}
		else $acumulador.=$linea."\n";
	}
	$escribir=true;
}
if($escribir){
	#echo $acumulador;
	exec('sudo chmod -R u=rwx,g=rwx,o=rwx '.$ruta);
	exec('sudo cat '.$ruta.$archivo.' > '.$ruta.$archivo.'.backup');
	exec('sudo rm '.$ruta.$archivo);
    $nuevo = fopen($ruta.$archivo, "a");
    fwrite($nuevo, trim($acumulador));
    fclose($nuevo);
	exec('sudo chmod -R u=rwx,g=rwx,o=rwx '.$ruta);
	exec('sudo chown root:root '.$ruta.$archivo.'.backup');
	exec('sudo chown root:root '.$ruta.$archivo);

	echo "Actualización correcta</br>";
	echo "Reiniciando servicios...</br>";
	exec('sudo .'.$ruta.$archivo,$xs0);
	foreach ($xs0 as $lxs0)print $lxs0."</br>";
	exec('sudo service xpertntc start',$xs);
	foreach ($xs as $lxs)print $lxs."</br>";
}
function mverificador($nombre){
	$nombre=str_replace(array(" ","."), "", $nombre);
	print '<input type="checkbox" id="chk_'.$nombre.'" name ="chk_'.$nombre.'"/>';
}
?>
<h2 align="center">Modificaciones personalizadas del firewall</h2>
<h3>IPs en <b><?php print $grupo; ?></b></h3>
<form id="frm_aip" name="frm_aip" method="post" action="">
Nueva IP: <input type="text" name="txt_nip" id="txt_nip">
<input type="hidden" name="haip" id="haip" value="agregar">
<button type="submit">Guardar</button>
</form>
<form id="frm_eip" name="frm_eip" method="post" action="">
<table>
	<?php
	exec('sudo cat '.$ruta.$archivo.' | grep "ipset -A '.$grupo.'" | awk \'{print $4}\'',$lista);
	foreach ($lista as $ip){
		print '<tr><td>';
		mverificador($grupo.trim($ip));
		print '</td><td>'.$ip.'</td></tr>';	
	} 
	 ?>
</table>
<input type="hidden" name="heip" id="heip" value="eliminar">
<button type="submit">Eliminar</button>
</form>
<hr>
<form name="regresar" action="index.php">
		<button type="submit" style="background-color: #d9534f;">Regresar</button>
</form>
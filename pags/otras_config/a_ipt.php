<?php
include_once("../../pags/cabecera.php");
#dir d rwx rwx rwx
#rch - rwx rwx rwx
#
#$ruta="/wwnetworks/xadmon/pags/otras_config/ap/";#acomodar rutas#"/wwnetworks/scripts/";
$ruta="/wwnetworks/scripts/";
$archivo="netup.sh";

$contenido=null;
$escribir=false;
$acumulador="";
$parametros=array("Bloquear puerto","Bloquear protocolo","Bloquear IP","Bloquear url-aplicacion","Bloquear cadena","Permitir puerto","Permitir protocolo","Permitir IP","Permitir url-aplicacion","Permitir cadena");
if(isset($_POST['lineas'])&&$_POST['lineas']!=0){
	exec('sudo cat '.$ruta.$archivo,$contenido);
	for ($i=0; $i <count($contenido) ; $i++) { 
		if(preg_match("/(#IPtables#)/", $contenido[$i])){
			$acumulador.=$contenido[$i]."\n";
			foreach ($parametros as $p) {
				$i++;
				$acumulador.=$contenido[$i]."\n";
				$t=explode(" ", trim($p));			
				$contador=-1;
				for ($l=0; $l < $_POST['lineas']; $l++) { 
					if(isset($_POST['txt_'.$l.'_'.trim($t[0]).trim($t[1])]))$contador++;
					if(isset($_POST['txt_'.$l.'_'.trim($t[0]).trim($t[1])])&&$_POST['txt_'.$l.'_'.trim($t[0]).trim($t[1])]!=""){
						$comentario=false;
						if(!isset($_POST['chk_'.$l.'_'.trim($t[0]).trim($t[1])]))$comentario=true;
						$acumulador.=mregla($comentario,trim($t[0]),trim($t[1]),$_POST['txt_'.$l.'_'.trim($t[0]).trim($t[1])])."\n";
					}
				}
				if(trim($t[1])!="cadena")$i=$i+2*$contador;
				else $i=$i+$contador;
			}
		}
		else $acumulador.=$contenido[$i]."\n";
	}
	$escribir=true;
}
if($escribir){
	#echo $acumulador;
	exec('sudo chmod -R u=rwx,g=rwx,o=rwx '.$ruta);
	exec('sudo cat '.$ruta.$archivo.' > '.$ruta.$archivo.'.backup');
	exec('sudo rm '.$ruta.$archivo);
    $nuevo = fopen($ruta.$archivo, "a");
    fwrite($nuevo, trim($acumulador));
    fclose($nuevo);
	exec('sudo chmod -R u=rwx,g=rwx,o=rwx '.$ruta);
	exec('sudo chown root:root '.$ruta.$archivo.'.backup');
	exec('sudo chown root:root '.$ruta.$archivo);

	echo "<b>Actualización correcta</b></br>";
	echo "Reiniciando servicios...</br>";;
	exec('sudo service xpertntc start',$xs);
	foreach ($xs as $lxs)print $lxs."</br>";
}
function mregla($comentario,$accion,$objetivo,$parametro){
	$regla="";
	$target="";
	$cadena="";
	if($accion=="Bloquear")$target="DROP";
	else if($accion="Permitir")$target="ACCEPT";
	switch ($objetivo) {
		case 'puerto':
			if($comentario){
				$regla="#iptables -A INPUT -p tcp --sport ".$parametro." -j ".$target."\n#iptables -A OUTPUT -p tcp --dport ".$parametro." -j ".$target;
			}
			else{
				$regla="iptables -A INPUT -p tcp --sport ".$parametro." -j ".$target."\niptables -A OUTPUT -p tcp --dport ".$parametro." -j ".$target;
			}
			break;
		case 'protocolo':
			if($comentario){
				$regla="#iptables -A INPUT -p ".$parametro." -j ".$target."\n#iptables -A OUTPUT -p ".$parametro." -j ".$target;
			}
			else{
				$regla="iptables -A INPUT -p ".$parametro." -j ".$target."\niptables -A OUTPUT -p ".$parametro." -j ".$target;
			}
			break;
		case 'IP':
			if(preg_match("/-/", $parametro)){
				if($comentario){
					$regla="#iptables -A INPUT -p tcp -m iprange --src-range ".$parametro." -j ".$target."\n#iptables -A OUTPUT -p tcp -m iprange --dst-range ".$parametro." -j ".$target;
				}
				else {
					$regla="iptables -A INPUT -p tcp -m iprange --src-range ".$parametro." -j ".$target."\niptables -A OUTPUT -p tcp -m iprange --dst-range ".$parametro." -j ".$target;
				}
			}
			else{
				if($comentario){
					$regla="#iptables -A INPUT -s ".$parametro." -j ".$target."\n#iptables -A OUTPUT -d ".$parametro." -j ".$target;
				}
				else {
					$regla="iptables -A INPUT -s ".$parametro." -j ".$target."\niptables -A OUTPUT -d ".$parametro." -j ".$target;
				}
			}
			break;
		case 'url-aplicacion':
			if($comentario){
				$regla="#iptables -A INPUT -p tcp -s ".$parametro." -j ".$target."\n#iptables -A OUTPUT -p tcp -d ".$parametro." -j ".$target;
			}
			else {
				$regla="iptables -A INPUT -p tcp -s ".$parametro." -j ".$target."\niptables -A OUTPUT -p tcp -d ".$parametro." -j ".$target;
			}
			break;
		case 'cadena':
			if($accion=="Bloquear"){
				if($comentario){
					$regla="#iptables -t filter -A FORWARD -i eth0 -m string --algo bm --string \"".$parametro."\" -j REJECT --reject-with icmp-proto-unreachable && echo \"string n D ok\"";
				}
				else {
					$regla="iptables -t filter -A FORWARD -i eth0 -m string --algo bm --string \"".$parametro."\" -j REJECT --reject-with icmp-proto-unreachable && echo \"string n D ok\"";
				}
			}
			else if($accion=="Permitir"){
				if($comentario){
					$regla="#iptables -t nat -I PREROUTING -d ".$parametro." -j ACCEPT";
				}
				else {
					$regla="iptables -t nat -I PREROUTING -d ".$parametro." -j ACCEPT";
				}
			}
			break;
		default:
			break;
	}
	return $regla;
}
function odatos($titulo,$renglon){
	$datos;
	$palabras=explode(" ", trim($renglon));
	switch ($titulo) {
		case 'puerto':
			$datos[0]="Puerto";
			for ($i=0; $i <count($palabras) ; $i++) if(trim($palabras[$i])=="--sport"){
				$datos[1]=$palabras[$i+1];
				break;
			}
			break;
		case 'protocolo':
			$datos[0]="Protocolo";
			for ($i=0; $i <count($palabras) ; $i++) if(trim($palabras[$i])=="-p"){
				$datos[1]=$palabras[$i+1];
				break;
			}
			break;
		case 'IP':
			if(preg_match("/(--src-range)/", $renglon)){
				$datos[0]="Rango";
				for ($i=0; $i <count($palabras) ; $i++) if(trim($palabras[$i])=="--src-range"){
					$datos[1]=$palabras[$i+1];
					break;
				}
			}
			else if(preg_match("/(\/)/", $renglon)){
				$datos[0]="Segmento";
				for ($i=0; $i <count($palabras) ; $i++) if(trim($palabras[$i])=="-s"){
					$datos[1]=$palabras[$i+1];
					break;
				}
			}
			else {
				$datos[0]="IP";
				for ($i=0; $i <count($palabras) ; $i++) if(trim($palabras[$i])=="-s"){
					$datos[1]=$palabras[$i+1];
					break;
				}
			}
			break;
		case 'url-aplicacion':
			$datos[0]="URL/Aplicación";
			for ($i=0; $i <count($palabras) ; $i++) if(trim($palabras[$i])=="-s"){
				$datos[1]=$palabras[$i+1];
				break;
			}
			break;
		case 'cadena':
			$datos[0]="Cadena de texto";
			if(preg_match("/(--string)/", $renglon))$marca="--string";
			else $marca="-d";
			for ($i=0; $i <count($palabras) ; $i++) if(trim($palabras[$i])==$marca){
				$datos[1]=str_replace("\"", "", $palabras[$i+1]);
				break;
			}
			break;
		default:
			# code...
			break;
	}
	return $datos;
}
function mverificador($l,$nombre,$comentario){
	$nombre=str_replace(" ", "_", $nombre);
	print ' Activar: <input type="checkbox" id="chk_'.$l.'_'.$nombre.'" name ="chk_'.$l.'_'.$nombre.'"';
	if(!$comentario)print ' checked="checked"';
	print '/>';
}
function mcaja($l,$nombre,$texto,$valor){
	print '<td>'.$texto.'</td><td> <input type="text" id="txt_'.$l.'_'.$nombre.'" name="txt_'.$l.'_'.$nombre.'" value="'.$valor.'" ></td>';
}
function mbtnb($l,$nombre){
	print '<input type="button" id="btn_'.$l.'_'.$nombre.'" name="btn_'.$l.'_'.$nombre.'" value="Borrar" onClick="document.getElementById(\'txt_'.$l.'_'.$nombre.'\').value=\'\';document.getElementById(\'txt_'.$l.'_'.$nombre.'\').style.visibility=\'hidden\';document.getElementById(\'btn_'.$l.'_'.$nombre.'\').style.visibility=\'hidden\'">';
}
function mnuevo($l,$nombre,$titulo,$valor,$comentario){
	print '<tr><td>';
	mverificador($l,$nombre,$comentario);
	print '</td>';
	if($valor==""){
		if($nombre!="IP")mcaja($l,$nombre,"Nuevo: ",$valor);
		else mcaja($l,$nombre,"Nuevo IP/Segmento/Rango: ",$valor);
	}
	else mcaja($l,$nombre,$titulo.": ",$valor);
	print '<td>';
	mbtnb($l,$nombre);
	print '</td></tr>';
}
?>
<h2 align="center">IPtables</h2>
<h3>Reglas IPtables (netup.sh)</b></h3>
<form id="frm_ipt" name="frm_ipt" method="post" action="">
<table>
	<?php
	$lineas=0;
	exec('sudo cat '.$ruta.$archivo,$renglones);
	for ($i=0; $i <count($renglones) ; $i++) { 
		if(preg_match("/(#IPtables#)/", $renglones[$i])){
			for ($i=$i+1; $i <count($renglones) ; $i++) {
				foreach ($parametros as $p) {
					if(preg_match("/(-".$p."-)/", $renglones[$i])){
						#echo $renglones[$i];
						print "<tr><td colspan=\"5\"><br><b>".$p."</b></td></tr>";
						$t=explode(" ", trim($p));
						if(!preg_match("/(^#{3,})/", $renglones[$i+1])){
							for ($i=$i+1; $i <count($renglones) ; $i++) {
								$od=odatos($t[1],$renglones[$i]);
								$comentario=false; 
								if(preg_match("/^#/", $renglones[$i]))$comentario=true;
								#echo $t[1]."-".$od[0]."-".$od[1]."-".$comentario."</br>";
								mnuevo($lineas++,$t[0].$t[1],$od[0],$od[1],$comentario);
								if($t[1]!="cadena"){
									$i++;
									$lineas++;
								}
								if(preg_match("/(^#{3,})/", $renglones[$i+1])){
									mnuevo($lineas++,$t[0].$t[1],$t[1],"",false);
									break;
								}
							}
						}
						else{
							mnuevo($lineas++,$t[0].$t[1],$t[1],"",false);
						}
						break;
					}
				}
				if(preg_match("/(#Fin IPtables#)/", $renglones[$i]))break;
			}
			break;
		}
	}
	?>
</table>
<input type="hidden" name="lineas" id="lineas" value="<?php print $lineas; ?>">
<button type="submit">Guardar</button>
</form>
<hr>
<form name="regresar" action="index.php">
		<button type="submit" style="background-color: #d9534f;">Regresar</button>
</form>
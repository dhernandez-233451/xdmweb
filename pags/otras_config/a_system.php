<?php
include_once("../../pags/cabecera.php");
#sys.c
#dir d rwx rwx rwx
#rch - rw- r-- r--
#pxy.cfg
#
#
$ruta="/wwnetworks/conf/";#"/wwnetworks/xadmon/pags/otras_config/ap/";
$archivo="system.conf";


#Recuperación############################
$tipo="system";							#
include_once("../../pags/lnk_rec.php"); #
#########################################
#$parametros=array(0 => 'NETWORKING_ON',1 => 'PROXY_TYPE',2 => 'IMQ_ON',3 => 'MODE');
#$p_opciones=array(array('TRUE','FALSE'),array('NONE','NORMAL','EXTERNAL','COMPRESSED'),array('TRUE','FALSE'),array('NONE','FO','LB'));
$parametros=array(0 => 'PROXY_TYPE',1 => 'MODE');
$parametrosxt=array(0 => 'Tipo de proxy o filtrado',1 => 'Modo de configuración');
$p_opciones=array(array('NONE','NORMAL'),array('NONE','FO','LB'));
function pxyconf($type){
	$r="/wwnetworks/conf/";#"/wwnetworks/xadmon/pags/otras_config/ap/";
	$a="proxy.configurado";
	exec('cat '.$r.$a.' > '.$r.$a.'.backup');
	exec('echo "'.$type.'" > '.$r.$a);
	exec('sudo chown root:root '.$r.$a);
}
if(isset($_POST['lineas'])&&$_POST['lineas']!=0){
	#dejar de mandar la lina a los constructores de objetos
	$contenido="";
	exec('cat '.$ruta.$archivo,$xc);
	foreach ($xc as $lxc){
		$permanencia=true;
		$renglon=$lxc;
		if(preg_match("/\#/", $lxc)){
			$renglon=str_replace(array('#','# '),'',$lxc);
			$renglon=str_replace("\t", '', $renglon);
		}
		if(preg_match("/(declare)/", $renglon)&&preg_match("/(WAN)/", $renglon)){
			$permanencia=false;
			if(!isset($_POST['chk__declare']))$contenido.='#';
			$contenido.="\t"."declare -a ";
			/*if(preg_match("/(LAN)/", $renglon))$red="LAN";
			else if(preg_match("/(WAN)/", $renglon))*/
			$red="WAN";
			$contenido.=$red."=(";
			for($j=0;$j<$_POST['cajas'];$j++)if(isset($_POST['txt__'.$red.'_'.$j])&&$_POST['txt__'.$red.'_'.$j]!="")$contenido.=$_POST['txt__'.$red.'_'.$j]." ";
			$contenido = substr($contenido, 0, -1);
			$contenido.=")\t\t\t\t\t#";
		}
		else for($i=0;$i<count($parametros);$i++)if(preg_match("/^".$parametros[$i]."/", $renglon)){
				$permanencia=false;
				if(!isset($_POST['chk__'.$parametros[$i]]))$contenido.='#';
				$contenido.=$parametros[$i]."=".$_POST['cb__'.$parametros[$i]]."\t\t\t\t\t#";
			}
		if($permanencia)$contenido.=$lxc;
		$contenido.="\n";
	}

	exec('sudo chmod -R u=rwx,g=rwx,o=rwx '.$ruta);
	exec('cat '.$ruta.$archivo.' > '.$ruta.$archivo.'.backup');
	exec('sudo rm '.$ruta.$archivo);
    $nuevo = fopen($ruta.$archivo, "a");
    fwrite($nuevo, $contenido);
    fclose($nuevo);
	pxyconf($_POST['cb__PROXY_TYPE']);
	exec('sudo chmod -R u=rwx,g=rwx,o=rwx '.$ruta);
	exec('sudo chown root:root '.$ruta.$archivo.'.backup');
	exec('sudo chown root:root '.$ruta.$archivo);

	#echo "Modificación aplicada \n Reiniciando servicios...";
	#código para reiniciar servicios
	echo "Actualización correcta";
	#print $contenido;
	$contenido="";
}

function metiqueta($nombre){
	print '<label>'.$nombre.'</label>';
}
function mcaja($l,$nombre,$valor,$i){
	print '<input type="text" id="txt_'.$l.'_'.$nombre.'_'.$i.'" name="txt_'.$l.'_'.$nombre.'_'.$i.'" value="'.$valor.'">';
}
function mverificador($l,$nombre,$comentario){
	print ' Activar: <input type="checkbox" id="chk_'.$l.'_'.$nombre.'" name ="chk_'.$l.'_'.$nombre.'"';
	if(!$comentario)print ' checked="checked"';
	print '/>';
}
function mnota($l,$cadena,$xc){
	echo $cadena;
	print '<input type="hidden" id="hdn_'.$l.'" name="hdn_'.$l.'" value="'.$xc.'" >';
}
function mlista($l,$nombre,$valor,$i,$a){
	$valor=str_replace(" ", "", $valor);
	print '<select id="cb_'.$l.'_'.$nombre.'" name="cb_'.$l.'_'.$nombre.'">';
	foreach ($a[$i] as $op){
		print '<option value="'.$op.'" ';
		if($valor==$op)print 'selected="selected" ';
		print ($nombre=="MODE"&&$op=="NONE")?'>ROUTER</option>':'>'.$op.'</option>';	
	} 
	print '</select>';
}

exec('cat '.$ruta.$archivo,$contenido);

$cajas=1;
$linea=0;
?>
<h2 align="center">Archivo de configuración</h2>
<h3>System.conf</h3>
<form id="frm_sys" name="frm_sys" method="post" action="">
<?php
foreach ($contenido as $lc) {
	print '<p>';
	$comentario=false;
	$renglon=$lc;
	if(preg_match("/\#/", $lc)){
		$renglon=str_replace(array('#','# '),'',$lc);
		$renglon=str_replace("\t", '', $renglon);
		if(preg_match("/^\#/", $lc))$comentario=true;
	}
	if(preg_match("/(declare)/", $renglon)&&preg_match("/(WAN)/", $renglon)){
		$palabras=explode("=", $renglon);
		mverificador("","declare",$comentario);
		metiqueta("Declarar ");
		/*if(preg_match("/(LAN)/", $palabras[0]))$red="LAN";
		else if(preg_match("/(WAN)/", $palabras[0]))*/
		$red="WAN";
		metiqueta($red);
		$palabra=str_replace(array('(',')'),'',$palabras[1]);
		$palabra=trim($palabra);
		$ifs=explode(" ", $palabra);
		for($j=0;$j<count($ifs);$j++)mcaja("",$red,$ifs[$j],$j);
		print "</br>Nuevo</br>";
		mcaja("",$red,"",(count($ifs)));
		if($cajas<count($ifs))$cajas=count($ifs);
	}
	else for($i=0;$i<count($parametros);$i++)if(preg_match("/^".$parametros[$i]."/", $renglon)){
			$palabras=explode("=", $renglon);
			mverificador("",$palabras[0],$comentario);
			metiqueta($parametrosxt[$i]);
			mlista("",$palabras[0],$palabras[1],$i,$p_opciones);
		}
	
	print '</p>';
	$linea++;
}
?>
<hr>
<input type="hidden" id ="lineas" name="lineas" value="<?php print $linea ?>">
<input type="hidden" id ="cajas" name="cajas" value="<?php print $cajas ?>">
<button type="submit">Guardar</button>
</form>
<hr>
<form name="regresar" action="index.php">
		<button type="submit" style="background-color: #d9534f;">Regresar</button>
</form>
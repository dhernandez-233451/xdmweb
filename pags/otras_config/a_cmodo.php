<?php
include_once("../../pags/cabecera.php");
#dir d rwx rwx rwx
#rch - rw- rw- rw-
$ruta_cfg="/wwnetworks/conf/";#"/wwnetworks/xadmon/pags/otras_config/ap/modo/";
$archivo_cfg="system.conf";
#dir d rwx r-x r-x
#rch - rw- r-- r--
$ruta_wfsc="/etc/wfs/";#"/wwnetworks/xadmon/pags/otras_config/ap/modo/";
$archivo_wfsc="wfs.conf";
#dir d rwx r-x r-x
#rch - rwx r-x r-x
$ruta_wfs="/usr/bin/";#"/wwnetworks/xadmon/pags/otras_config/ap/modo/";
$archivo_wfs="wfs";
#dir d rwx rwx rwx
#rch - rwx rwx rwx
$ruta_lb="/wwnetworks/scripts/";#"/wwnetworks/xadmon/pags/otras_config/ap/modo/";
$archivo_lb="loadBalancer.conf";
$modo="";



$parametros;
$parametrosxt;
$archivo;
$p_wfs=array(0 => 'PRIMARY_GW',1 => 'SECONDARY_GW'/*,2 => 'PRIMARY_DEV',3 => 'SECONDARY_DEV',4 => 'MAIL_TARGET',5 => 'MAIL_TARGET2',6 => 'PRIMARY_CMD',7 => 'SECONDARY_CMD',8 => 'MAX_LATENCY',9 => 'INTERVAL',10 => 'TEST_COUNT',11 => 'THRESHOLD',12 => 'COOLDOWNDELAY01',13 => 'COOLDOWNDELAY02',14 => 'TARGETS_FILE',15 => 'DAEMON',16 => 'QUIET',17 => 'PIDFILE',18 => 'DEBUG'*/);
$p_wfsxt=array('IP del gawteray primario','IP del gateway secundario');
$p_lb=array(0 => 'NAME',1 => 'DEVICE',2 => 'WAN',3 => 'Gateway',4 => 'Weight');
$p_lbxt=array('Nombre de la interfaz','Interfaz fisica','IP del gateway','','Peso');

function mconfig($m,$ruta,$archivo){
	$acumulador="";
	exec('cat '.$ruta.$archivo,$texto);
	foreach ($texto as $lt){
		if(preg_match("/^(MODE=)/", $lt))$acumulador.="MODE=".$m."\n";
		else $acumulador.=$lt."\n";
	}
	exec('sudo chmod -R u=rwx,g=rwx,o=rwx '.$ruta);
	exec('cat '.$ruta.$archivo.' > '.$ruta.$archivo.'.backup');
	exec('sudo rm '.$ruta.$archivo);
    $nuevo = fopen($ruta.$archivo, "a");
    fwrite($nuevo, $acumulador);
    fclose($nuevo);
    exec('sudo chmod -R u=rwx,g=rwx,o=rwx '.$ruta);
	exec('sudo chown root:root '.$ruta.$archivo.'.backup');
	exec('sudo chown root:root '.$ruta.$archivo);

	echo "<b>Actualización correcta</b></br>";
	echo "<b><i>Reiniciando servicios...</i></b></br>";
    print shell_exec('/etc/init.d/xpertntc start')."</br>";
}

if(isset($_POST['lineas'])&&$_POST['lineas']!=0){
	$modo=$_POST['modo'];
	$contenido="";
	if($modo=="FO"){
		$parametros=$p_wfs;
		$parametrosxt=$p_wfsxt;
		$archivo=$ruta_wfsc.$archivo_wfsc;
		$archivo0=$ruta_wfs.$archivo_wfs;
	}
	elseif ($modo=="LB"){
		$parametros=$p_lb;
		$parametrosxt=$p_lbxt;
		$archivo=$ruta_lb.$archivo_lb;
	} 
	if($modo=="FO"){
		$ncontenido="";
		exec('cat '.$archivo0,$cx);
		$cant_parametros=0;
		for($i=0;$i<count($cx);$i++) {
			$permanece=true;
			while($cant_parametros<count($parametros)){
				foreach ($parametros as $p) {
					if(preg_match("/(".$p.")/", $cx[$i])){
						for ($k=0; $k < $_POST['cajas']; $k++) { 
							if(isset($_POST['txth__'.$p.'_'.$k])){
								$ncontenido.="\t".$p."=".$_POST['txth__'.$p.'_'.$k]."\n";
								$cant_parametros+=1;
								$permanece=false;
								break;
							}
						}
						break;
					}
				}
				if($permanece)$ncontenido.=$cx[$i]."\n";
				$i++;
				$permanece=true;
			}
			if($permanece)$ncontenido.=$cx[$i];
			if($i<count($cx))$ncontenido.="\n";
		}
		#print $ncontenido;
		exec('sudo chmod u=rwx,g=rwx,o=rwx '.$ruta_wfs);
		exec('sudo chmod u=rwx,g=rwx,o=rwx '.$archivo0);
		exec('cat '.$archivo0.' > '.$archivo0.'.backup');
		exec('sudo rm '.$archivo0);
    	$nuevo = fopen($archivo0, "a");
    	fwrite($nuevo, $ncontenido);
    	fclose($nuevo);
    	exec('sudo chmod u=rwx,g=rwx,o=rwx '.$archivo0.'.backup');
    	exec('sudo chmod u=rwx,g=rwx,o=rwx '.$archivo0);
    	exec('sudo chown root:root '.$archivo0.'.backup');
		exec('sudo chown root:root '.$archivo0);
	}
	for ($i=0; $i < $_POST['lineas']; $i++) {
		if(isset($_POST['hdn_'.$i])){
			$nc=str_replace('_cmll_', '\"', $_POST['hdn_'.$i]);
			$contenido.=$nc;
		}
		if($modo=="FO")for ($j=0; $j < count($parametros); $j++) { 
			if($j>=4&&$j<=7){
				for ($k=0; $k < $_POST['cajas']; $k++)if(isset($_POST['txt_'.$i.'_'.$parametros[$j].'_'.$k])){
					if(!isset($_POST['chk_'.$i.'_'.$parametros[$j]]))$contenido.="#";
					if($k==0)$contenido.='\"'.$_POST['txt_'.$i.'_'.$parametros[$j].'_'.$k].'\"';
					else if($k==1)$contenido.= $parametros[$j].'=\"'.$_POST['txt_'.$i.'_'.$parametros[$j].'_'.$k].'\"';
					else $contenido.='\"'.$_POST['txt_'.$i.'_'.$parametros[$j].'_'.$k].'\"';
				}
			}
			else{
				for ($k=0; $k < $_POST['cajas']; $k++)if(isset($_POST['txt_'.$i.'_'.$parametros[$j].'_'.$k])){
					if(!isset($_POST['chk_'.$i.'_'.$parametros[$j]]))$contenido.="#";
					if($k==0)$contenido.=$_POST['txt_'.$i.'_'.$parametros[$j].'_'.$k];
					else if($k==1) $contenido.= $parametros[$j]."=".$_POST['txt_'.$i.'_'.$parametros[$j].'_'.$k];
					else $contenido.=" ".$_POST['txt_'.$i.'_'.$parametros[$j].'_'.$k];
				}
			}
		}
		if($modo=="LB"){
			if($i==0){
				$contenido.="\n";
				for ($j=0; $j < count($parametros); $j++){
					if($j==0)$contenido.="#".$parametros[$j]." \t";
					elseif($j==2){
						$contenido.=$parametros[$j]." ".$parametros[$j+1]." \t";
						$j++;
					}
					elseif($j==(count($parametros)-1))$contenido.=$parametros[$j];
					else $contenido.=$parametros[$j]." \t";
				}
			}
			for($k=0;$k<$_POST['cajas'];$k++)if(isset($_POST['txt_'.$i.'__'.$k])&&$_POST['txt_'.$i.'__'.$k]!=""){
					if(!isset($_POST['chk_'.$i.'_'])&&$k==0)$contenido.="#";
					if($k==0)$contenido.=$_POST['txt_'.$i.'__'.$k]."=>"."\t";
					else if($_POST['txt_'.$i.'__'.$k]=="1")$contenido.=$_POST['txt_'.$i.'__'.$k];
					else $contenido.=$_POST['txt_'.$i.'__'.$k]."\t";
				}
		}
		if($modo!="LB"||$i!=1)$contenido.="\n";
	}
	if($modo=="FO")$contenido=substr($contenido, 0,-1);
	
	$rta="";
	if($modo=="FO")$rta=$ruta_wfsc;
	elseif($modo=="LB")$rta=$ruta_lb;

	exec('sudo chmod u=rwx,g=rwx,o=rwx '.$rta);
	exec('sudo chmod u=rw,g=rw,o=rw '.$archivo.'.backup');
	exec('sudo chmod u=rw,g=rw,o=rw '.$archivo);
	exec('cat '.$archivo.' > '.$archivo.'.backup');
	$instruccion='echo "'.$contenido.'" > '.$archivo;
	exec($instruccion);
	exec('sudo chmod u=rwx,g=rwx,o=rwx '.$archivo.'.backup');
	exec('sudo chmod u=rwx,g=rwx,o=rwx '.$archivo);
	exec('sudo chown root:root '.$archivo.'.backup');
	exec('sudo chown root:root '.$archivo);

	mconfig($modo,$ruta_cfg,$archivo_cfg);
	#print $contenido;
	$contenido="";
}
if(isset($_GET['m'])){
	if($_GET['m']=="router"){
		$modo="NONE";
		mconfig($modo,$ruta_cfg,$archivo_cfg);
	}
	else if($_GET['m']=="failover"){
		$modo="FO";
		$parametros=$p_wfs;
		$parametrosxt=$p_wfsxt;
		$archivo=$ruta_wfsc.$archivo_wfsc;
	}
	else if($_GET['m']=="loadbalancing"){
		$modo="LB";
		$parametros=$p_lb;
		$parametrosxt=$p_lbxt;
		$archivo=$ruta_lb.$archivo_lb;
	}
}

function metiqueta($nombre){
	print '<label>'.$nombre.'</label>';
}
function mcaja($l,$nombre,$valor,$i){
	print '<input type="text" id="txt_'.$l.'_'.$nombre.'_'.$i.'" name="txt_'.$l.'_'.$nombre.'_'.$i.'" value="'.$valor.'">';
}
function mcajab($l,$valor,$i){
	print '<input type="text" readonly="readonly" id="txt_'.$l.'__'.$i.'" name="txt_'.$l.'__'.$i.'" value="'.$valor.'">';
}
function mco($nombre,$valor,$i){
	print '<input type="hidden" id="txth__'.$nombre.'_'.$i.'" name="txth__'.$nombre.'_'.$i.'" value="'.$valor.'">';
}
function mverificador($l,$nombre,$comentario){
	print ' Activar: <input type="checkbox" id="chk_'.$l.'_'.$nombre.'" name ="chk_'.$l.'_'.$nombre.'"';
	if(!$comentario)print ' checked="checked"';
	print '/>';
}
function mnota($l,$cadena,$xc){
	#echo $cadena;
	print '<input type="hidden" id="hdn_'.$l.'" name="hdn_'.$l.'" value="'.$xc.'" >';
}

exec('cat '.$archivo,$contenido);

$cajas=1;
$linea=0;
?>
<h2 align="center">MODO</h2>
<h3><?php print ($modo=="NONE")?"ROUTER":$modo; ?></h3>
<form id="frm_cmodo" name="frm_cmodo" method="post" action="">
<?php
if($modo=="FO")foreach ($contenido as $lc) {
	print '<p>';
	$renglon=str_replace("\t", '', $lc);
	$comentario=false;
	if(preg_match("/\#/", $lc)){
		$renglon=str_replace('# ','#',$lc);
		$renglon=str_replace("\t", '', $renglon);
		$paux=explode("#", $renglon);
		$renglon=$paux[count($paux)-1];
		$comentario=true;
	}
	$comillas=false;
	if(preg_match("/=/", $renglon)){
		if(preg_match("/\"/", $renglon)){
			$comillas=true;
			$renglon=str_replace('"','',$renglon);
		}
		$palabras=explode("=", $renglon);
	}
	else $palabras=explode(" ", $renglon);
	if($cajas<count($palabras))$cajas=count($palabras);
	$palabra=$palabras[0];
	$nota=true;
	for ($i=0; $i <count($parametros) ; $i++) if($palabra==$parametros[$i]){
			$nota=false;
			mverificador($linea,$palabra,$comentario);
			if(count($palabras)>1){
				metiqueta($parametrosxt[$i]);
				for ($j=1; $j <count($palabras) ; $j++){
					 mcaja($linea,$palabra,$palabras[$j],$j);
					 mco($palabra,$palabras[$j],$j);
				}
			}
			else{
				mcaja($linea,$palabra,$palabra,0);
				mco($palabra,$palabras[$j],$j);
			} 
			break;
		}
	if($nota){
		$xlc=str_replace('"', '_cmll_', $lc);
		mnota($linea,$lc,$xlc);
	} 
	print '</p>';
	$linea++;
}
#if($modo=="FO")print '<hr><a href="a_targetcm.php" target="_blank"><i>Ir a targets</i></a>';
print '<table border="0">';
if($modo=="LB")foreach ($contenido as $lc) {
	print '<tr>';
	$renglon=str_replace("\t", ' ', $lc);
	$comentario=false;
	if(preg_match("/\#/", $lc)){
		$renglon=str_replace("\t", ' ', $lc);
		$paux=explode("#", $renglon);
		$renglon=$paux[count($paux)-1];
		$comentario=true;
	}
	$comillas=false;
	if(preg_match("/=>/", $renglon))$renglon=str_replace('=>','',$renglon);
	$palabras=explode(" ", $renglon);
	if($cajas<count($palabras))$cajas=count($palabras);
	$nota=true;
	for ($i=0; $i <count($palabras); $i++){
		$palabra=$palabras[$i];
		if($palabra!=""){
			for($j=0;$j<count($parametros);$j++)if($palabra==$parametros[$j]){
				$nota=false;
				if($palabra=="NAME"){
					print '<td>';
					print '</td>';
				}
				print '<td>';
				if(@$palabras[$i+1]==@$parametros[$j+1]){
					metiqueta($parametrosxt[$j]." "/*.$palabras[$i+1]*/);
					$j++;
					$i++;
				}
				else metiqueta($parametrosxt[$j]);
				print '</td>';
				break;
			}
			if(preg_match("/((WAN)\d)|((eth)\d)|(((\d){1,3})\.((\d){1,3})\.((\d){1,3})\.((\d){1,3}))|(\d)/", $palabra)){
				if(preg_match("/(WAN)\d/", $palabra)){
					print '<td>';
					mverificador($linea,"",$comentario);
					print '</td>';
				}
				$nota=false;
				print '<td>';
				if(preg_match("/((WAN)\d)|((eth)\d)/", $palabra))mcajab($linea,$palabra,$i);
				else mcaja($linea,"",$palabra,$i);
				print '</td>';
			}
		} 
	}
	if($nota){
		$xlc=str_replace('"', '_cmll_', $lc);
		mnota($linea,$lc,$xlc);
	}
	print '</tr>';
	$linea++;
}
print '</table>';
?>
<hr>
<input type="hidden" id ="lineas" name="lineas" value="<?php print $linea ?>">
<input type="hidden" id ="cajas" name="cajas" value="<?php print $cajas ?>">
<input type="hidden" id ="modo" name="modo" value="<?php print $modo ?>">
<button type="submit">Guardar</button>
</form>
<hr>
<form name="regresar" action="index.php">
		<button type="submit" style="background-color: #d9534f;">Regresar</button>
</form>
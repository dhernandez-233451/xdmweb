###########################################################################################################################################################################################
#														#
###------------------------------------------------------- ACCEPT: PERMITIR FACEBOOK Y TWITTER PARA ESTA LISTA DE IPS --------------------------------------------------------------------#
#														#
/sbin/iptables -t filter -A FORWARD -i eth0 -m set --match-set facebook_twitter_src src -m set --match-set facebook_twitter_dst dst -j ACCEPT && echo "set fb A ok"							#
#														#
###------------------------------------------------------------ DROP: DENEGAR FACEBOOK Y TWITTER PARA TODOS ------------------------------------------------------------------------------#
#														#
/sbin/iptables -t filter -A FORWARD -i eth0 -m set --match-set facebook_twitter_dst dst -m comment --comment "DROP: twitter, facebook" -j REJECT  && echo "set fb D ok"						#
#														#
###------------------------------------------- ACCEPT/DROP : Permitir o Aceptar YouTube, Netflix para esta lista de IPs ------------------------------------------------------------------#
#														#
#/sbin/iptables -t filter -A FORWARD -i eth0 -m set --match-set string_users_src src -m string --algo bm --string "login.live.com" -j ACCEPT && echo "set y A ok"								#
#/sbin/iptables -t filter -A FORWARD -i eth0 -m set --match-set string_users_src src -m string --algo bm --string "mail.google.com" -j ACCEPT && echo "set n A ok"							#
#/sbin/iptables -t filter -A FORWARD -i eth0 -m set --match-set string_users_src src -m string --algo bm --string "mail.yahoo.com" -j ACCEPT && echo "set n A ok"								#
#														#
###-------------------------------------------------------- DROP : DENEGAR YouTube, Netflix para TODOS -----------------------------------------------------------------------------------#
#														#
#/sbin/iptables -t filter -A FORWARD -i eth0 -m string --algo bm --string "login.live.com" -j REJECT --reject-with icmp-proto-unreachable && echo "string y D ok"								#
#/sbin/iptables -t filter -A FORWARD -i eth0 -m string --algo bm --string "mail.google.com" -j REJECT --reject-with icmp-proto-unreachable && echo "string n D ok"							#
#/sbin/iptables -t filter -A FORWARD -i eth0 -m string --algo bm --string "mail.yahoo.com" -j REJECT --reject-with icmp-proto-unreachable && echo "string n D ok"								#
#														#
###########################################################################################################################################################################################



echo "*************************************************Cever ByPass (Destino) ******************************************************"
#ByPass Cever
/sbin/iptables -t nat -A PREROUTING -d 207.128.0.0/14 -j ACCEPT #HONDA IP RANGE
/sbin/iptables -t nat -A PREROUTING -d 164.109.25.194 -j ACCEPT #honda.com
/sbin/iptables -t nat -A PREROUTING -d 148.244.196.196 -j ACCEPT #www.caemsa.com.mx
/sbin/iptables -t nat -A PREROUTING -d 148.244.196.195 -j ACCEPT #www.caemsa.com.mx

/sbin/iptables -t nat -A PREROUTING -d 174.142.53.14 -j ACCEPT #itcautocirc.intelisis-solutions.com has address 174.142.53.14
/sbin/iptables -t nat -A PREROUTING -d 70.38.58.180 -j ACCEPT #itcautocirc.intelisis-solutions.com has address 70.38.58.180
/sbin/iptables -t nat -A PREROUTING -d 70.38.58.181 -j ACCEPT #itcautocirc.intelisis-solutions.com has address 70.38.58.181
/sbin/iptables -t nat -A PREROUTING -d 70.38.58.182 -j ACCEPT #itcautocirc.intelisis-solutions.com has address 70.38.58.182

/sbin/iptables -t nat -A PREROUTING -d 23.229.160.0/19 -j ACCEPT        #Humansite
/sbin/iptables -t nat -A PREROUTING -d 64.151.64.0/19 -j ACCEPT #Humansite
/sbin/iptables -t nat -A PREROUTING -d 66.235.128.0/19 -j ACCEPT        #Bancomer
/sbin/iptables -t nat -A PREROUTING -d 207.248.129/24 -j ACCEPT #Grupo Inbursa
/sbin/iptables -t nat -A PREROUTING -d 184.107.183.138 -j ACCEPT #intelisis
/sbin/iptables -t nat -A PREROUTING -d cever.humansite.com.mx -j ACCEPT
/sbin/iptables -t nat -A PREROUTING -d 137.135.0.0/16 -j ACCEPT #office 365
/sbin/iptables -t nat -A PREROUTING -d 132.245.0.0/16 -j ACCEPT #office 365
/sbin/iptables -t nat -A PREROUTING -d 148.244.0.0/18 -j ACCEPT #Bancomer bancomerplanpiso.com.mx
/sbin/iptables -t nat -A PREROUTING -d 101.1.39.126 -j ACCEPT ##http://wpc.mobis.co.kr/Login.jsp
/sbin/iptables -t nat -A PREROUTING -d 211.216.0.0/13 -j ACCEPT ##http://wpc.mobis.co.kr/Login.jsp
/sbin/iptables -t nat -A PREROUTING -d 207.248.252.0/24 -j ACCEPT ##http://qvaluaciones.qualitas.com.mx/portalQ-web/actions/main/MainAction.action
/sbin/iptables -t nat -A PREROUTING -d 212.54.128.0/20 -j ACCEPT ##http://logon.kaseya.net/Default.aspx
/sbin/iptables -t nat -A PREROUTING -d 23.204.0.135 -j ACCEPT #login.ifmsystems.com esta alojado en Akamai Technologies
/sbin/iptables -t nat -A PREROUTING -d 58.87.37.51 -j ACCEPT #https://ddms.kia.co.kr
/sbin/iptables -t nat -A PREROUTING -d 200.66.80.0/20 -j ACCEPT #cf.bbvaleasing.mx
/sbin/iptables -t nat -A PREROUTING -d 200.57.38.181 -j ACCEPT ##sistema de enseñanza abierta a distancia -colegeio de bachilleres 03 - EdoMex
/sbin/iptables -t nat -A PREROUTING -d 201.175.12.0/24 -j ACCEPT ##www.sisetdtmac.com.mx
/sbin/iptables -t nat -A PREROUTING -d 200.36.106.28 -j ACCEPT #segurosgnp
/sbin/iptables -t nat -A PREROUTING -d 23.253.232.116 -j ACCEPT #seminuevos.com
/sbin/iptables -t nat -A PREROUTING -d 201.175.33.140 -j ACCEPT #segundamano.com.mx
/sbin/iptables -t nat -A PREROUTING -d 64.70.56.99 -j ACCEPT #www.autoplaza.com
/sbin/iptables -t nat -A PREROUTING -d 64.4.0.0/18 -j ACCEPT #microsoft
/sbin/iptables -t nat -A PREROUTING -d 187.141.32.0/19 -j ACCEPT #wbc2.burodecredito.com.mx:543/RceOnline/index.html
/sbin/iptables -t nat -A PREROUTING -d 187.128.0.0/12 -j ACCEPT #finanziauto.com.mx
/sbin/iptables -t nat -A PREROUTING -d 168.61.0.0/16 -j ACCEPT #http://autoasignacion.audatex.com.mx/autoasignacion/
/sbin/iptables -t nat -A PREROUTING -d 157.56.0.0/16 -j ACCEPT #http://autoasignacion.audatex.com.mx/autoasignacion/
/sbin/iptables -t nat -A PREROUTING -d 63.115.41.0/24 -j ACCEPT #https://www.int.audatexsolutions.com/?csglo
#/sbin/iptables -t nat -A PREROUTING -d 207.131.207.28 -j ACCEPT #www.in.honda.com
#/sbin/iptables -t nat -A PREROUTING -d 207.131.207.58 -j ACCEPT #www.in.honda.com

#microsoft
/sbin/iptables -t nat -A PREROUTING -d microsoftonline.com -j ACCEPT
/sbin/iptables -t nat -A PREROUTING -d login.microsoftonline.com -j ACCEPT
/sbin/iptables -t nat -A PREROUTING -d microsoft.com -j ACCEPT
/sbin/iptables -t nat -A PREROUTING -d login.live.com -j ACCEPT
/sbin/iptables -t nat -A PREROUTING -d azure.com -j ACCEPT
/sbin/iptables -t nat -A PREROUTING -d office.com -j ACCEPT
/sbin/iptables -t nat -A PREROUTING -d office365.com -j ACCEPT

/sbin/iptables -t nat -A PREROUTING -d 58.87.49.0/24 -m comment --comment "http://dpos.mobismexico.com" -j ACCEPT ##http://dpos.mobismexico.com
/sbin/iptables -t nat -A PREROUTING -d 199.67.180.0/24 -m comment --comment "https://www.banamex.com/" -j ACCEPT ##https://www.banamex.com/
/sbin/iptables -t nat -A PREROUTING -d 184.28.156.0/23 -m comment --comment "Santander" -j ACCEPT ##Santander
/sbin/iptables -t nat -A PREROUTING -d 23.35.96.0/20 -m comment --comment "Scotiabank" -j ACCEPT ##Scotiabank
/sbin/iptables -t nat -A PREROUTING -d 23.222.28.0/23 -m comment --comment "Scotiabank" -j ACCEPT ##Scotiabank
/sbin/iptables -t nat -A PREROUTING -d 200.33.84.0/24 -m comment --comment "Sat" -j ACCEPT ##Sat
/sbin/iptables -t nat -A PREROUTING -d facturacioncapufe.com.mx -m comment --comment "facturacioncapufe" -j ACCEPT #facturacioncapufe.com.mx
#######################################################################################################
echo "*************************************************Cever ByPass (Fuente) ******************************************************"
#ByPass Cever
/sbin/iptables -t nat -A PREROUTING -s x.x.x.x -j ACCEPT #Ejemplo fuente
/sbin/iptables -t nat -A PREROUTING -s 192.168.1.x -j ACCEPT #Ejemplo fuente
######ENMASCARANDO TUNEL############

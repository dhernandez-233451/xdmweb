#/bin/bash

#Tor Blacklist Updater
#Nico


#************************************************
#EN ESTA LISTA SE ACTUALIZA CADA 30 MINUTOS Y CONTIENE TODAS LAS IPS DE TOR SIN REPETIRSE
#curl https://www.dan.me.uk/torlist/ > torlist


#************************************************

ruta=/wwnetworks/scripts/listas
#Borrando Lista Anterior Tor_query_EXPORT.csv
rm $ruta/Tor_query_EXPORT.csv
rm $ruta/tor_all

#EN ESTA SE DESCARGA DE LA LISTA ACTIVA Y SE FILTRA SOLO EL PUERTO 443
echo "Descargando Tor_query_EXPORT"
wget http://torstatus.blutmagie.de/query_export.php/Tor_query_EXPORT.csv -O $ruta/Tor_query_EXPORT.csv

echo "Extrayendo IP's y guardando en /wwnetworks/scripts/listas/tor"
cat $ruta/Tor_query_EXPORT.csv |grep -E -o "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)" |sed -e '/^0.2./d' > $ruta/tor_all

cat $ruta/tor_all |sort |uniq > $ruta/tor

#habilitar para puerto 443 solamente
###cat $ruta/Tor_query_EXPORT.csv |grep ",443," |grep -E -o "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)" |sed -e '/^0.2./d' > $ruta/torblacklist_443

#grep -w -E -o '^(25[0-4]|2[0-4][0-9]|1[0-9][0-9]|[1]?[1-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)'

echo "*************************************************************************************"
echo "Agregando Nodos Activos de Tor a la lista negra...usando IPSET"
ipset --flush tor
for tor in `sed '/#.*/d' /wwnetworks/scripts/listas/tor`;
  do
     	#echo "Agregando Nodos IPHASH $tor"
    	ipset -A tor $tor
done

echo "La lista negra de TOR se ha actualizado."
echo ""
echo "Ultima actualizacion TOR Blacklist: $(date)" >> /tmp/tor_updater.log
echo ""


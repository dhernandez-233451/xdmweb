<?php
	include_once("../cabecera.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Opciones Xpert</title>
</head>
<body>
<h2 align="center">Opciones de administración</h2>
	<table border="0">
		<tr>
			<td>
				<?php if(permiso($_SESSION["nivel"],1)) { print '<a href="menu.php"><h4>*Ir a la administración del Xpert</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],2)) { print '<a href="usuarios.php"><h4>Administrar usuarios</h4></a></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],3)) { print '<a href="sec/licencia.php"><h4><i>Licencia de uso...</i></h4></a>';} ?>
			</td>
		</tr>
	</table>
</body>
</html>

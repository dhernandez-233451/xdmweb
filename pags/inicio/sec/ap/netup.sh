#!/bin/bash
. /wwnetworks/conf/system.conf
echo ""
echo "***********************************************************************"
# "Interfaz de entrada y salida, ips de entrada y salida"
for laninfo in ${!LAN[*]}
   do
      echo "LAN INTERFACE: ${LAN[$laninfo]} - LAN IP: ${LAN_IP[$laninfo]}"
   done
echo "-----------------------------------------------------------------------"
for waninfo in ${!WAN[*]}
   do
      echo "WAN INTERFACE: ${WAN[$waninfo]} - WAN IP: ${WAN_IP[$waninfo]}"
   done
echo "***********************************************************************"
echo ""
echo "Limpiando directivas de IPTables..."
/sbin/iptables -F
/sbin/iptables -X
/sbin/iptables -Z
/sbin/iptables -t nat -F
/sbin/iptables -t nat -X
/sbin/iptables -t nat -Z
/sbin/iptables -t mangle -F
/sbin/iptables -t mangle -X
/sbin/iptables -t mangle -Z

/sbin/iptables -N accounting && echo "Chain forward accounting OK"
/sbin/iptables -N fail2ban-ssh && echo "Chain fail2ban-ssh OK"
/sbin/iptables -N LOGGING && echo "Chain filter LOGGING OK"
/sbin/iptables -t nat -N LOGGING && echo "Chain nat LOGGING OK"
/sbin/iptables -t mangle -N LOGGING && echo "Chain mangle LOGGING OK"

/sbin/iptables -A FORWARD -j accounting
/sbin/iptables -A INPUT -p tcp --dport 9022 -j fail2ban-ssh
/sbin/iptables -A INPUT -j LOGGING
/sbin/iptables -A OUTPUT -j LOGGING
/sbin/iptables -A FORWARD -j LOGGING
/sbin/iptables -t nat -A PREROUTING -j LOGGING
/sbin/iptables -t mangle -A PREROUTING -j LOGGING


#INPUT Modo DROP
/sbin/iptables -P INPUT DROP
#Allow ESTABLISHED, RELATED Connections
/sbin/iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
#Allow Established, Related Connections for VPN
/sbin/iptables -A FORWARD -i tun+ -m state --state ESTABLISHED,RELATED -j ACCEPT
# Allow unlimited traffic on loopback
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

#Allow DNS Traffic and ICMP
for lan in ${LAN[*]}; do
	/sbin/iptables -A INPUT -i $lan -p tcp -m tcp --dport 53 -m state --state NEW -j ACCEPT
	/sbin/iptables -A INPUT -i $lan -p udp -m udp --dport 53 -m state --state NEW -j ACCEPT
	/sbin/iptables -A INPUT -i $lan -p icmp -m limit --limit 1/s --limit-burst 1 -j ACCEPT
done

#Habiliatar IP Accounting por IP
#for ((i=1;i<=254;i++))
#    do
#        /sbin/iptables -A accounting -d 192.168.255.$i
#        /sbin/iptables -A accounting -s 192.168.255.$i
#    done



echo "<Anti DDos> ************************************************************************************************************************************************************************"
#DDos mangle IPtables -
#/sbin/iptables -t mangle -I LOGGING -m limit --limit 5/min --limit-burst 5 -j ULOG --ulog-prefix "LOGGING MANGLE"
### 1: Drop invalid packets ###
/sbin/iptables -t mangle -A PREROUTING -m conntrack --ctstate INVALID -j DROP

### 2: Drop TCP packets that are new and are not SYN ###
/sbin/iptables -t mangle -A PREROUTING -p tcp ! --syn -m conntrack --ctstate NEW -j DROP

### 3: Drop SYN packets with suspicious MSS value ###
/sbin/iptables -t mangle -A PREROUTING -p tcp -m conntrack --ctstate NEW -m tcpmss ! --mss 536:65535 -j DROP

### 4: Block packets with bogus TCP flags ###
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,SYN FIN,SYN -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,ACK FIN -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,URG URG -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,FIN FIN -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,PSH PSH -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL ALL -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL NONE -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL FIN,PSH,URG -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL SYN,FIN,PSH,URG -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j DROP

### 5: Block spoofed packets ###
/sbin/iptables -t mangle -A PREROUTING -s 224.0.0.0/3 -j DROP
/sbin/iptables -t mangle -A PREROUTING -s 169.254.0.0/16 -j DROP
#/sbin/iptables -t mangle -A PREROUTING -s 172.16.0.0/12 -j DROP
#/sbin/iptables -t mangle -A PREROUTING -s 192.0.2.0/24 -j DROP
#/sbin/iptables -t mangle -A PREROUTING -s 192.168.0.0/16 -j DROP
#/sbin/iptables -t mangle -A PREROUTING -s 10.0.0.0/8 -j DROP
/sbin/iptables -t mangle -A PREROUTING -s 0.0.0.0/8 -j DROP
/sbin/iptables -t mangle -A PREROUTING -s 240.0.0.0/5 -j DROP
/sbin/iptables -t mangle -A PREROUTING -s 127.0.0.0/8 ! -i lo -j DROP

### 6: Drop ICMP (useless protocol) ###
/sbin/iptables -t mangle -A PREROUTING -p icmp -m limit --limit 1/s --limit-burst 1 -j ACCEPT
#/sbin/iptables -t mangle -A PREROUTING -p icmp -j DROP #Drop ICMP

### 7: Drop fragments in all chains ###
/sbin/iptables -t mangle -A PREROUTING -f -j DROP

### 8: Limit connections per source IP ###
/sbin/iptables -A INPUT -p tcp -m connlimit --connlimit-above 111 -j REJECT --reject-with tcp-reset

### 9: Limit RST packets ###
/sbin/iptables -A INPUT -p tcp --tcp-flags RST RST -m limit --limit 2/s --limit-burst 2 -j ACCEPT
/sbin/iptables -A INPUT -p tcp --tcp-flags RST RST -j DROP

### 10: Limit new TCP connections per second per source IP ###
#/sbin/iptables -A INPUT -p tcp -m conntrack --ctstate NEW -m limit --limit 60/s --limit-burst 20 -j ACCEPT
#/sbin/iptables -A INPUT -p tcp -m conntrack --ctstate NEW -j DROP

### 11: Use SYNPROXY on all ports (disables connection limiting rule) ###
#/sbin/iptables -t raw -D PREROUTING -p tcp -m tcp --syn -j CT --notrack
#/sbin/iptables -D INPUT -p tcp -m tcp -m conntrack --ctstate INVALID,UNTRACKED -j SYNPROXY --sack-perm --timestamp --wscale 7 --mss 1460
#/sbin/iptables -D INPUT -m conntrack --ctstate INVALID -j DROP
echo "</Anti DDos> ************************************************************************************************************************************************************"

############################################################################## Incoming Traffic ###########################################################################################
for index in ${!WAN[*]}										#
   do													#
		echo "ACCEPT/DROP Incoming Traffic Ports"				#
		##Allow SSH Incoming Traffic							#
		/sbin/iptables -A LOGGING -i ${WAN[$index]}  -p tcp --dport 9022 -m comment --comment "LOG SSH" -m limit --limit 10/min --limit-burst 3 -j ULOG --ulog-prefix "LOG SSH"					#
		/sbin/iptables -A INPUT -i ${WAN[$index]}  -p tcp --dport 9022 -m comment --comment "SSH" -j ACCEPT								#
#------------------------------------------------------------------------ Puertos conocidos - Root Ports ---------------------------------------------------------------------------------#
		/sbin/iptables -A LOGGING -i ${WAN[$index]} -p tcp --dport 1:1024 -m comment --comment "LOGGING TCProot" -m limit --limit 5/min --limit-burst 2 -j ULOG --ulog-prefix "LOG TCProotDrop" #
		/sbin/iptables -A LOGGING -i ${WAN[$index]} -p udp --dport 1:1024 -m comment --comment "LOGGING UDProot" -m limit --limit 5/min --limit-burst 2 -j ULOG --ulog-prefix "LOG UDProotDrop" #
		/sbin/iptables -A INPUT -i ${WAN[$index]} -p tcp --dport 1:1024 -m comment --comment "TCProotDrop" -j DROP							#
		/sbin/iptables -A INPUT -i ${WAN[$index]} -p udp --dport 1:1024 -m comment --comment "UDProotDrop" -j DROP							#
#------------------------------------------------------------------------ Dropping All Incoming Traffic ----------------------------------------------------------------------------------#
		iptables -A LOGGING -i ${WAN[$index]} -m comment --comment "LOG INPUT DROP ${WAN[$index]}" -m limit --limit 5/min --limit-burst 2  -j ULOG --ulog-prefix "LOG INPUT DROP "	#
		iptables -A INPUT -i ${WAN[$index]} -m comment --comment "INPUT DROP ALL ${WAN[$index]} " -j DROP									##
done	# </END for index>									#
###########################################################################################################################################################################################

########################################################################### INBOUND CONNECTIONS ###########################################################################################
#													#
echo "Aceptando nuevas conexiones excepto para las Interfaces WAN..."		#
#													#
for array_wan in ${WAN[*]}									#
	do
	### 10: Limit new TCP connections per second per source IP ###
		/sbin/iptables -A INPUT -p tcp -m conntrack --ctstate NEW ! -i $array_wan -m limit --limit 60/s --limit-burst 20 -j ACCEPT || echo "Error: INPUT NEW ! $array_wan"
		/sbin/iptables -A INPUT -p tcp -m conntrack --ctstate NEW -j DROP
	#	/sbin/iptables -A INPUT -m state --state NEW ! -i $array_wan -m comment --comment "INPUT NEW ! $array_wan" -j ACCEPT || echo "Error: INPUT NEW ! $array_wan"								#
	done												#
#													#
###########################################################################################################################################################################################


############################################################ Forzando query DNS de toda la LAN al DNS Local ###############################################################################
#Habilitar si se quiere redireccionar el trafico DNS				#
##iptables -t nat -A PREROUTING -i eth0 ! -d 10.20.1.254 -p udp --dport 53 -m comment --comment "Local DNS Query" -j DNAT --to 10.20.1.254:53			#
##iptables -t nat -A PREROUTING -i eth0 ! -d 10.20.1.254 -p tcp --dport 53 -m comment --comment "Local DNS Query" -j DNAT --to 10.20.1.254:53			#
#													#
###########################################################################################################################################################################################


###########################################################################################################################################################################################
#													#
###------------------------------------------------------------------------- DROP: Bloqueando Nodos Tor ----------------------------------------------------------------------------------#
#													#
/sbin/iptables -t filter -A FORWARD -i eth0 -m set --match-set tor dst -m comment --comment "DROP: TOR Exit Nodes" -j DROP && echo "set tor D ok"		#
#													#
###------------------------------------------------------- ACCEPT: PERMITIR FACEBOOK Y TWITTER PARA ESTA LISTA DE IPS --------------------------------------------------------------------#
#
#
#/sbin/iptables -t filter -A FORWARD -i eth0 -m set --match-set facebook_twitter_src src -m set --match-set facebook_twitter_dst dst -j ACCEPT && echo "set fb A ok"
#
#
###------------------------------------------------------------ DROP: DENEGAR FACEBOOK Y TWITTER PARA TODOS ------------------------------------------------------------------------------#
#
#
#/sbin/iptables -t filter -A FORWARD -i eth0 -m set --match-set facebook_twitter_dst dst -m comment --comment "DROP: twitter, facebook" -j REJECT  && echo "set fb D ok"
#
#
###------------------------------------------- ACCEPT/DROP : Permitir o Aceptar YouTube, Netflix para esta lista de IPs ------------------------------------------------------------------#
#
#
#/sbin/iptables -t filter -A FORWARD -i eth0 -m set --match-set string_users_src src -m string --algo bm --string www.youtube.com -j ACCEPT && echo "set y A ok"
#/sbin/iptables -t filter -A FORWARD -i eth0 -m set --match-set string_users_src src -m string --algo bm --string www.netflix.com -j ACCEPT && echo "set y A ok"
#
#
###-------------------------------------------------------- DROP : DENEGAR YouTube, Netflix para TODOS -----------------------------------------------------------------------------------#
#
#
#/sbin/iptables -t filter -A FORWARD -i eth0 -m string --algo bm --string www.youtube.com -j REJECT --reject-with icmp-proto-unreachable && echo "string y D ok"
#/sbin/iptables -t filter -A FORWARD -i eth0 -m string --algo bm --string www.netflix.com -j REJECT --reject-with icmp-proto-unreachable && echo "string y D ok"
#
#
###########################################################################################################################################################################################
#
#############################Filtrado personalizado###############################
####--------------------------por listas (DROP/ACCEPT)------------------------####
/sbin/iptables -t filter -A FORWARD -i eth0 -m set --match-set dropbox_users src -m string --algo bm --string "www.youtube.com" -j ACCEPT && echo "set y A ok"
/sbin/iptables -t filter -A FORWARD -i eth0 -m set --match-set dropbox_users src -m string --algo bm --string "www.fb.com" -j DROP && echo "set y A ok"
/sbin/iptables -t filter -A FORWARD -i eth0 -m set --match-set hotmail_users src -m string --algo bm --string "www.google.com" -j ACCEPT && echo "set y A ok"
####--------------------------general (REJECT)--------------------------------####
/sbin/iptables -t filter -A FORWARD -i eth0 -m string --algo bm --string "www.twitter.com" -j REJECT --reject-with icmp-proto-unreachable && echo "string n D ok"
/sbin/iptables -t filter -A FORWARD -i eth0 -m string --algo bm --string "www.otro.com" -j REJECT --reject-with icmp-proto-unreachable && echo "string n D ok"
####--------------------------general (ACCEPT)--------------------------------####
/sbin/iptables -t nat -I PREROUTING -d www.youtube.com -j ACCEPT
/sbin/iptables -t nat -I PREROUTING -d www.yoump3.eu -j ACCEPT
#############################Fin filtardo personalizado###########################
#
################################################################## ESTABLISHED, RELATED CONNECTIONS #######################################################################################
#													#
echo "Aceptando Conexiones ESTABLECIDAS y RELACIONADAS..."			#
for array_lan in ${LAN[*]}									#
	do												#
   	for array_wan in ${WAN[*]}								#
   		do										#
				/sbin/iptables -A FORWARD -i $array_wan -o $array_lan -m state --state ESTABLISHED,RELATED -m comment --comment "Aceptando Conexiones $array_wan" -j ACCEPT || echo "Error: Aceptando conexiones en $array_wan"		#
				/sbin/iptables -A FORWARD -i $array_lan -o $array_wan -m comment --comment "Aceptando FORWARD $array_wan" -j ACCEPT || echo "Error: Aceptando FORWARD $array_wan"					#
			done										#
	done												#
###########################################################################################################################################################################################


############################################################################### SNAT ######################################################################################################
echo "Activando NAT..."										#
for index in ${!WAN[*]}										#
   do													#
		/sbin/iptables -t nat -A POSTROUTING -o ${WAN[$index]} -j SNAT --to ${WAN_IP[$index]} -m comment --comment "SNAT WAN: ${WAN[$index]}" || echo "Error: SNAT WAN: ${WAN[$index]}"		#
	done												#
###########################################################################################################################################################################################


############################################################################ Post Routes NAT ##############################################################################################
echo "Agregando post-routes NAT..."								#
for array_wan in ${WAN[*]}									#
   do													#
		/sbin/iptables -t nat -A POSTROUTING -o $array_wan -j MASQUERADE -m comment --comment "post-routes MASQ - WAN: $array_wan" || echo "Error: post-routes MASQ - WAN: $array_wan"		#
		/sbin/iptables -A FORWARD -i $array_wan -o $array_wan -j REJECT -m comment --comment "post-routes REJECT - WAN: $array_wan" || echo "Error: post-routes REJECT - WAN: $array_wan"	#
	done												#
###########################################################################################################################################################################################


#######################################################################  N A T  ###########################################################################################################
#AQUI VAN TODAS LAS REDIRECCIONES								  #
#/sbin/iptables -t nat -A PREROUTING -p TCP --dport 9022 -m comment --comment "comentario NAT" -j DNAT --to 10.20.1.131:9022 #Comentario NAT			  #
#													  #
###########################################################################################################################################################################################

echo "Iniciando NAT..."
echo 1 > /proc/sys/net/ipv4/ip_forward
##Para cuando se habilita un FTP a traves de NAT
#modprobe ip_conntrack_ftp
#modprobe ip_nat_ftp
##PAra aceptar conexiones por tuneles pptp
modprobe nf_conntrack_pptp
modprobe nf_conntrack_proto_gre
##SIP a traves de NAT
modprobe ip_conntrack_sip
modprobe ip_nat_sip

echo "Activando modulo de tracking de conexiones en el kernel..."
/sbin/sysctl net.netfilter.nf_conntrack_acct=1

echo "Inicio de red terminado."
echo ""

<?php
	include_once("../cabecera.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Administración Xpert</title>
</head>
<body>

<center>
	<table border="0" width="80%">
		<tr>
			<td align="center">
				<?php if(permiso($_SESSION["nivel"],4)) { print '<a href="../consola_admon/acciones.php?accion=1"><h4>Interfaces de red</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],5)) { print '<a href="../consola_admon/viftop.php" target="_blank"><h4>Tráfico de red</h4></a></a>';} ?>
				<!--<a href="pags/consola_admon/acciones.php?accion=3"><h4>Tráfico de red jnettop</h4></a>
				<a href="pags/consola_admon/acciones.php?accion=4"><h4>Tráfico general</h4></a>-->
				<?php if(permiso($_SESSION["nivel"],6)) { print '<a href="../consola_admon/acciones.php?accion=5" target="_blank"><h4>Test de velocidad</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],7)) { print '<a href="../consola_admon/acciones.php?accion=7"><h4>IP pública</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],8)) { print '<a href="../consola_admon/acciones.php?accion=6"><h4>Rutas</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],9)) { print '<a href="../consola_admon/acciones.php?accion=9"><h4>Procesos</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],10)) { print '<a href="../consola_admon/acciones.php?accion=8"><h4>Mensages del Kernel</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],11)) { print '<a href="../consola_admon/acciones.php?accion=15"><h4>Tabla Nat del firewall</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],12)) { print '<a href="../consola_admon/acciones.php?accion=16"><h4>Tabla filter del firewall</h4></a>';} ?>
			</td>
			<td> </td>
			<td align="center">
				<?php if(permiso($_SESSION["nivel"],13)) { print '<a href="../consola_admon/acciones.php?accion=10"><h4>Realizar ping</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],14)) { print '<a href="../consola_admon/acciones.php?accion=11"><h4>Realizar traza</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],15)) { print '<a href="../consola_admon/acciones.php?accion=12"><h4>Reiniciar servicio de red</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],16)) { print '<a href="../consola_admon/acciones.php?accion=13"><h4>Reiniciar servicio de DCHP</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],17)) { print '<a href="../consola_admon/acciones.php?accion=14"><h4>Reiniciar servicio de VPN</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],21)) { print '<a href="../ed_av/acceso.php"><h4>Editar Scripts</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],20)) { print '<a href="../consola_admon/acciones.php?accion=17"><h4>Configurar Ruteo</h4></a>';} ?>
				<!--<a href="filtrado/filtrado.php"><h4>Filtrado de contenido</h4></a>-->
				<?php if(permiso($_SESSION["nivel"],22)) { print '<a href="../filtradodg/"><h4>Filtrado de contenido</h4></a>';} ?>
				<?php if(permiso($_SESSION["nivel"],23)) { print '<a href="../otras_config/"><h4>Otras configuraciones</h4></a>';} ?>
			</td>
		</tr>
		<tr>
			<td align="center">
				<?php if(permiso($_SESSION["nivel"],18)) { print '<a href="../consola_admon/acciones.php?accion=20"><h4>Reiniciar servidor</h4></a>';} ?>
				<!--<a href="phpseclib/cnxroot.php?a=2"><h4>Reiniciar servidor</h4></a>-->
			</td>
			<td></td>
			<td align="center">
				<?php if(permiso($_SESSION["nivel"],19)) { print '<a href="../consola_admon/acciones.php?accion=21"><h4>Apagar servidor</h4></a>';} ?>
				<!--<a href="phpseclib/cnxroot.php?a=3"><h4>Apagar servidor</h4></a>-->
			</td>
		</tr>
	</table>
</center>
</body>
</html>

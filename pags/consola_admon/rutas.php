<?php
include_once("../cabecera.php");
$titulo = array(0 =>  "",
	1=>"Agregar ruta",
	2=>"Eliminar ruta"); 

	if(isset($_POST['rbenviar']))if($_POST['rbenviar']=="Device"){
			$rag=shell_exec(' sudo route add -net '.$_POST['txtsegmento'].' netmask '.$_POST['txtmascara'].' dev '.$_POST['txtdevice']);
			if($rag!="")echo "Ruta: ".$_POST['txtsegmento']."/".$_POST['txtmascara']." sobre la interfaz ".$_POST['txtdevice'].", agregada";
			else echo $rag;
	}	
	else{
		$rag=shell_exec('sudo route add -net '.$_POST['txtsegmento'].' netmask '.$_POST['txtmascara'].' gw '.$_POST['txtgateway']);
		if($rag!="")echo "Ruta: ".$_POST['txtsegmento']."/".$_POST['txtmascara']." sobre el gateway".$_POST['txtgateway'].", agregada";
		else echo $rag;
	}
if(isset($_POST['rel'])){
	$partes=explode(" ", $_POST['rel']);
	$p;
	foreach ($partes as $pp) if($pp!="")$p[]=$pp;
	$rel="";
	if($p[3]=="UG") $rel=shell_exec('sudo route del -net '.$p[0].' netmask '.$p[2].' gw '.$p[1]);
	else $rel=shell_exec('sudo route del -net '.$p[0].' netmask '.$p[2].' dev '.$p[7]);
	if($rel!="")if($p[3]=="UG")echo "Ruta: ".$p[0]."/".$p[2]." sobre el gateway ".$p[1].", eliminada satisfactoriamente";
		else echo "Ruta: ".$p[0]."/".$p[2]." sobre la interfaz ".$p[7].", eliminada satisfactoriamente";
}

?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $titulo[$_GET['operacion']] ?></title>
</head>
<body>
<?php
	

if(isset($_GET['operacion'])){
	if($_GET['operacion']==1){
		print '
			<h2 align "center">Configurar nueva ruta</h2>
			<h3>Rutas exitentes:</h3>';
		exec('sudo route',$vre);
		foreach ($vre as $l_vre) echo $l_vre."</br>";
		print '
		<h3>Introduzca los datos necesarios</h3>
		<form id="fagrt" name="fagrt" method="post" action="rutas.php?operacion='.$_GET['operacion'].'">
			<label for "txtsegmento">Nombre del segmento: </label>
			<input type="text" id="txtsegmento" name="txtsegmento"></br>
			<label for "txtmascara">Nombre de la mascara: </label>
			<input type="text" id="txtmascara" name="txtmascara"></br>
			Forma de enviar la ruta</br>
			<input type="radio" name="rbenviar" id="rbenviar" value="Device" checked="checked">Device</br>
			<input type="radio" name="rbenviar" id="rbenviar" value="Gateway">Gateway</br>
			<label for "txtdevice">Introduzca el device: </label>
			<input type="text" id="txtdevice" name="txtdevice"></br>
			<label for "txtgateway">Nombre del gateway: </label>
			<input type="text" id="txtgateway" name="txtgateway"></br>
			<button type="submit">Guardar</button>
		</form>
		';
	}
	else if($_GET['operacion']==2){
		print '
			<h2 align "center">Eliminar ruta</h2>
			<h3>Rutas exitentes:</h3>';
			exec('sudo route',$rep);
			foreach ($rep as $l_rep)print $l_rep."</br>";
		print '
		<h3>Eliga para eliminar</h3>
		<form id="felrt" name="felrt" method="post" action="rutas.php?operacion='.$_GET['operacion'].'">
			<select id="rel" name="rel" >';
		exec('sudo route',$re);
		foreach ($re as $l_re){
			if(!preg_match("/(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/", $l_re))echo $l_re."</br>";
			else print '<option value="'.$l_re.'">'.$l_re.'</option>';
		} 
		print '
		</select>
			<button type="submit">Eliminar</button>
		</form>
		';
	}
}
?>
</br>
<a href="acciones.php?accion=17"><button type="button" style="background-color: #d9534f;">Regresar</button></a>
<!--<form name="regresar" action="acciones.php?accion=17">
	<button type="submit">Aceptar</button>
</form>-->
</body>
</html>
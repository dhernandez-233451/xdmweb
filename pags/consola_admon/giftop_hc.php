<?php
session_start();
exec('sudo /wwnetworks/iftop-1.0pre3/iftop -i '.$_POST['valorCaja1'].' -t -s 5s |grep "rate"',$resultado); 
foreach ($resultado as $linea) {
    if(preg_match("/and/", $linea)){
        $palabras=explode(" ", $linea);
        $contand=0;
        foreach ($palabras as $p) {
            if(($p!="")&&preg_match("/\d/", $p)){
                $contand++;
                if($contand==3){
                    if(preg_match("/Kb/", $p)){
                        $xvalor=explode("K", $p);
                        $valor=trim($xvalor[0]);
                        $_SESSION["tr"][]=($valor*1024);
                    }
                    else if(preg_match("/Mb/", $p)){
                        $xvalor=explode("M", $p);
                        $valor=trim($xvalor[0]);
                        $_SESSION["tr"][]=($valor*pow(1024,2));
                    }
                    else if(preg_match("/b/", $p)){
                        $xvalor=explode("b", $p);
                        $valor=trim($xvalor[0]);
                        $_SESSION["tr"][]=$valor;
                    }
                    #echo "--".$valor."-</br>";
                    break;
                }
            }
        }
    }
    else if(preg_match("/Peak/", $linea)){
        $palabras=explode(" ", $linea);
        $contpk=0;
        foreach ($palabras as $p) {
            if(($p!="")&&preg_match("/\d/", $p)){
                $contpk++;
                if($contpk==3){
                    if(preg_match("/Kb/", $p)){
                        $xvalor=explode("K", $p);
                        $valor=trim($xvalor[0]);
                        $_SESSION["mr"][]=($valor*1024);
                    }
                    else if(preg_match("/Mb/", $p)){
                        $xvalor=explode("M", $p);
                        $valor=trim($xvalor[0]);
                        $_SESSION["mr"][]=($valor*pow(1024,2));
                    }
                    else if(preg_match("/b/", $p)){
                        $xvalor=explode("b", $p);
                        $valor=trim($xvalor[0]);
                        $_SESSION["mr"][]=$valor;
                    }
                    #echo "--".$valor."-</br>";
                    break;
                }
            }
        }
    }
}
$k=0;
if(count($_SESSION["tr"])>40){
    foreach ($_SESSION["tr"] as $value) {
        $k=key($_SESSION["tr"]);
        break;
    }
    unset($_SESSION["tr"][$k-1]);
    unset($_SESSION["mr"][$k-1]);
}
print "Tasa de envío y recepción: ".$_SESSION["tr"][count($_SESSION["tr"])+$k-1]." bits</br>";
print "Tasa de pico: ".$_SESSION["mr"][count($_SESSION["mr"])+$k-1]." bits</br>";
?>
<script language="JavaScript">
/*var t=0;
var v=[0];
var interval=null;
function agregar(){
    v.push(Math.floor(Math.random() * 100));
    t=t+1;
    graf();
}
//agregar();
interval=setInterval('agregar()',1000);*/
graf();
</script>
<HTML>
<BODY>


<?php
/*$valoresArray;
$timeArray;
for($i = 0 ;$i<100;$i++){
    $valoresArray[$i]= (rand(0,100)*100);
    $time= $i;
    $timeArray[$i] = $time;
    echo "v: ".$valoresArray[$i].", t: ".$timeArray[$i]."</br>";
}*/
?>
<div id="contenedor"></div>

<!--<script src="https://code.jquery.com/jquery.js"></script>-->
    <!-- Importo el archivo Javascript de Highcharts directamente desde su servidor -->
<!--<script src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>-->
<script src="../../js/jquery-3.2.1.js"></script>
<script src="../../js/highcharts.js"></script>
<script src="../../js/exporting.js"></script>

<script>
function graf(){
    var f = new Date();
chartCPU = new Highcharts.Chart({//StockChart,Highcharts
    chart: {
        renderTo: 'contenedor'
        //defaultSeriesType: 'spline'
        //type: 'area'
        
    },
    rangeSelector : {
        enabled: false
    },
    title: {
        text: 'Gráfica de consumo de ancho de banda'
    },
    xAxis: {
        type: "category",
        //type: 'linear'
        title: {
                text: 'Segundos'
            },
            //tickInterval: 1
        //tickPixelInterval: 150,
        //maxZoom: 20 * 1000
    },
    yAxis: {
        minPadding: 0.2,
        maxPadding: 0.2,
        title: {
            text: 'Bits',
            margin: 10
        }
    },
    plotOptions: {
            series: {
                marker: {
                enabled: false
            }
        }
    },
    series: [{
        name: 'Tasa total',
        type: 'area',
        color: '#FF9B36',
        animation: false,
        showInNavigator: true,
        data: (function() {
                var data = [];
                <?php for($i = $k;$i<(count($_SESSION["tr"])+$k);$i++){ ?>
                data.push([<?php echo ($i*5);?>,<?php echo $_SESSION["tr"][$i];?>]);
                <?php } ?>
                /*for(var i=0; i<t; i++){
                    //data.push([Date.UTC(f.getFullYear(), f.getMonth(), f.getDate(), f.getHours(),f.getMinutes(),f.getSeconds(),f.getMilliseconds()),v[i]]);
                    data.push([i,v[i]]);
                }*/
                return data;
            })()
    },{   
        name: 'Tasa de pico',
        type: 'line',
        color: '#203B86',
        data: (function(){
            var data=[];
                <?php for($i = $k ;$i<(count($_SESSION["tr"])+$k);$i++){ ?>
                data.push([<?php echo ($i*5);?>,<?php echo $_SESSION["mr"][$i];?>]);
                <?php } ?>
            /*var s=0;
            for (var j=0; j<t; j++){
                s+=v[j];
            }
            var m=s/t;
            for (var k=0; k<t; k++){
                //data.push([Date.UTC(f.getFullYear(), f.getMonth(), f.getDate(), f.getHours(),f.getMinutes(),f.getSeconds(),f.getMilliseconds()),m]);
                data.push([k,m]);
            }*/
            return data;
        })()
    }],
    credits: {
            enabled: false
    },
    lang : {
        printChart: 'Imprimir',
        downloadJPEG: 'Descargar en JPEG',
        downloadPDF:  'Descargar en PDF',
        downloadPNG:  'Descargar en PNG',
        downloadSVG: '',
    }
});
}
</script>   
</BODY>

</html>
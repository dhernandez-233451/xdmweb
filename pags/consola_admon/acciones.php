<?php
include_once("../cabecera.php");
	function interfaces_red(){
		exec('/sbin/ifconfig',$ir);
		return $ir;
	}
	function test_velocidad(){
		exec('sudo speedtest-cli --share',$tv);
		return $tv;
	}
	function rutas(){
		return shell_exec('sudo ip route ls');
	}
	function ip_publica(){
		#return shell_exec('curl ipinfo.io/ip');
		$r=getenv('REMOTE_ADDR');
		echo "Tu dirección IP es: {$r} <hr>";
 
		#$s=getenv('HTTP_X_FORWARDED_FOR');
		#$s=$_SERVER['HTTP_X_FORWARDED_FOR'];
		#$s=$HTTP_SERVER_VARS["HTTP_X_FORWARDED_FOR"];
		$s =shell_exec('curl ipinfo.io/ip');
		echo "Tu dirección IP pública es: <b>{$s}</b><hr>";

		echo "El nombre del servidor es: {$_SERVER['SERVER_NAME']}<hr>"; 
		$h=getenv('HTTP_REFERER');	
		echo "Vienes procedente de la página: {$h}<hr>"; 
		echo "Te has conectado usando el puerto: {$_SERVER['REMOTE_PORT']}<hr>"; 
		echo "El agente de usuario de tu navegador es: {$_SERVER['HTTP_USER_AGENT']}";
	}
	function mensajes_kernel(){
		exec('sudo dmesg | tail -n 10',$mk);
		return $mk;
	}
	function procesos(){
		exec('sudo top -b -n 1',$pcs);
		return $pcs;
	}
	function ping($if,$host){
		exec('sudo ping -I '.$if.' -c 5 '.$host,$png);
		return $png;
	}
	function traza($host){
		exec('sudo mtr -r -c 5 '.$host,$trz);
		return $trz;
	}
	function reiniciar_red(){
		exec('sudo /etc/init.d/networking restart',$rr);
		return $rr;
	}
	function reiniciar_dhcp(){
		exec('sudo /etc/init.d/dhcp3-server restart',$rdhcp);
		return $rdhcp;
	}
	function reiniciar_vpn(){
		exec('sudo service openvpn restart',$rvpn);
		return $rvpn;
	}
	function tabla_nat(){
		exec('sudo iptables -t nat -nvL |more',$tnat);
		return $tnat;
	}
	function tabla_filter(){
		exec('sudo iptables -t filter -nvL |more',$tfltr);
		return $tfltr;
	}
	function reiniciar(){
		shell_exec('sudo reboot');
		echo "Reiniciando XpertNTC, espere unos minutos para volver a operar...";
	}
	function apagar(){
		#shell_exec('shutdown -h now');
		exec('sudo poweroff',$ap);
		foreach ($ap as $l_ap)echo $l_ap."</br>";
		echo "Apagando XpertNTC, para volver a configurarlo debe encender el servidor fisicamente";
	}
	if(isset($_GET['accion'])){
		$titulo;
		switch ($_GET['accion']) {
			case 1:
				$titulo="Interfaces de red";
				break;
			case 2:
				$titulo="Trafico de red (iftop)";
				break;
			case 3:
				$titulo="Trafico de red (jnettop)";
				break;
			case 4:
				$titulo="Trafico general";
				break;
			case 5:
				$titulo="Test de velocidad";
				break;
			case 6:
				$titulo="Rutas";
				break;
			case 7:
				$titulo="IP publica";
				break;
			case 8:
				$titulo="Mensajes del kernel";
				break;
			case 9:
				$titulo="Procesos";
				break;
			case 10:
				$titulo="Realizar ping";
				break;
			case 11:
				$titulo="Realizar traza";
				break;
			case 12:
				$titulo="Reiniciar servicio de red";
				break;
			case 13:
				$titulo="Reiniciar servicio de DHCP";
				break;
			case 14:
				$titulo="Reiniciar servicio VPN";
				break;
			case 15:
				$titulo="Tabla NAT del firewall";
				break;
			case 16:
				$titulo="Tabla filtrer del firewall";
				break;
			case 17:
				$titulo="Configurar ruteo";
				break;
			case 18:
				$titulo="Filtrado de contenido";
				break;
			case 19:
				$titulo="Otras configuraciones";
				break;
			case 20:
				$titulo="Reiniciar";
				break;
			case 21:
				$titulo="Apagar";
				break;
			default:
				# code...
				break;
		}
	}
?>
<!DOCTYPE html>
<html>
</br>
	<title>
		<?php print $titulo; ?>
	</title>
</head>
<body>
	<?php
	

		if(isset($_GET['accion'])){
			switch ($_GET['accion']) {
			case 1:
			foreach (interfaces_red() as $l_ir) echo $l_ir."</br>";
				break;
			case 2:
				#Sustituido
				break;
			case 3:
				#No necesario
				break;
			case 4:
				echo shell_exec('iptraf');
				break;
			case 5:
				$rtv=test_velocidad();
				for ($i=2; $i <9 ; $i++) echo $rtv[$i]."</br>";
				break;
			case 6:
				echo rutas();
				break;
			case 7:
				ip_publica();
				break;
			case 8:
				foreach (mensajes_kernel() as $l_mk) echo $l_mk."</br>";
				break;
			case 9:
				foreach (procesos() as $l_pcs) echo $l_pcs."</br>";
				break;
			case 10:
				print '
					<form id="fpng" name="fpng" method="post" action="acciones.php">
						<label for "txtinterfaz">Nombre de la interfaz: </label>
						<input type="text" id="txtinterfaz" name="txtinterfaz"></br>
						<label for "txtdominio">Nombre del dominio: </label>
						<input type="text" id="txtdominio" name="txtdominio"></br>
						<button type="submit">Ver</button>
						<input type="hidden" id="hpng" name ="hpng" value="p" >
					</form>
				';
				break;
			case 11:
				print '
					<form id="ftrz" name="ftrz" method="post" action="acciones.php">
						<label for "txtdominio">Nombre del dominio: </label>
						<input type="text" id="txtdominio" name="txtdominio">
						<button type="submit">Ver</button>
						<input type="hidden" id="htrz" name ="htrz" value="t" >
					</form>
				';
				break;
			case 12:
				foreach (reiniciar_red() as $l_rr) echo $l_rr."</br>";
				break;
			case 13:
				#print '<i>No ejecutar, puede tumbar la red de la oficina</i>';
				foreach (reiniciar_dhcp() as $l_rdhcp) echo $l_rdhcp."</br>";
				break;
			case 14:
				foreach (reiniciar_vpn() as $l_rvpn) echo $l_rvpn."</br>";
				break;
			case 15:
				foreach (tabla_nat() as $l_tnat) echo $l_tnat."</br>";
				break;
			case 16:
				foreach (tabla_filter() as $l_tfltr)echo $l_tfltr."</br>";
				break;
			case 17:
				print '
					<h2 align="center">Configurador de rutas</h2>
					<h3 align="center">Seleccione alguna de las operaciones a realizar</h3>
					<center>
						<table border=0 align="center" width="50%">
							<tr>
								<td>
									<p><a href="rutas.php?operacion=1">Agregar</a></p>
									<p><a href="rutas.php?operacion=2">Eliminar</a></p>
								</td>
							</tr>
						</table>
					</center>
				';
				break;
			case 18:
				print '
				<p><a href="../../filtrado/filtrado.php">Ir a filtrado de Contenido</a></p>
				';
				#header('Location: ../../filtrado/filtrado.php');
				break;
			case 19:
				print '
				<p><a href="../otras_config/">Ir a networking</a></p>
				';
				break;
			case 20:
				reiniciar();
				break;
			case 21:
				apagar();
				break;
			default:
				# code...
				break;
			}
		}
		if (isset($_POST['hpng'])) foreach (ping($_POST['txtinterfaz'],$_POST['txtdominio']) as $l_png) echo $l_png."</br>";
		if (isset($_POST['htrz'])) foreach (traza($_POST['txtdominio']) as $l_trz) echo $l_trz."</br>";
	?>
	<form name="regresar" action="../../index1.php">
		<button type="submit" style="background-color: #d9534f;">Regresar</button>
	</form>
</body>
</html>